import glob, os, cv2, time, tqdm
from file_utils import *
from detection_utils import *
from drawing_utils import draw_output

def runDetection( movieLoader, detector, drawer ):
    start_time = time.time()
    for frame_number in tqdm.tqdm(range( movieLoader.maxMovieFrame )):
        movieLoader.readFrame()
        detector.inference( movieLoader.currentFrame, movieLoader.frameId )
        drawer.draw( movieLoader.currentFrame, detector.detection_results.getDetectionResults )

    detector.outputJSON()
    total_time_taken = time.time() - start_time
    print(f"Time : {int(total_time_taken)}")
    print(f"FPS  : {int(total_time_taken)/movieLoader.maxMovieFrame}")

def main():
    movie_dir   = "reid_accuracy/camA/CameraA_20211118_0730-0740_C.MP4"
    weights_dir = "detection_utils/weights/yolov5s6.pt"
    base_color  = (5, 250, 247)
    loaded_file = file_loader.FileLoader( movie_dir )
    detection_  = detect_.Detect_( weights_dir )
    detection_.setupOutputJSON( loaded_file )
    drawing_ = draw_output.DetectionDraw( base_color )
    runDetection( loaded_file.movieLoader, detection_, drawing_ )

if __name__ == '__main__':
    main()
    os._exit(0)

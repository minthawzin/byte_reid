import cv2, os, numpy

class DrawOutput:

    @property
    def bboxColor( self ):
        return self.__bbox_color

    @property
    def lineThickness( self ):
        return self.__line_thickness

    @property
    def fontType( self ):
        return self.__font_type

    @property
    def fontScale( self ):
        return self.__font_scale

    def __init__( self, bbox_color ):
        self.__bbox_color     = bbox_color
        self.__line_thickness = 2
        self.__font_type      = cv2.FONT_HERSHEY_SIMPLEX
        self.__font_scale     = 0.75
        self._endVideo        = False
        self.display_height   = 1080
        self.display_width    = 1920
        fourcc                = cv2.VideoWriter_fourcc('M','P','4','V')
        self.out_video        = cv2.VideoWriter(f'output.MP4', fourcc, 30.0, (self.display_width, self.display_height))

    def draw( self, input_image, detection_results ):
        pass

    def display( self, display_image, window_name="output"):
        self.display_image = cv2.resize( display_image, (self.display_width, self.display_height) )
        cv2.imshow( window_name, self.display_image )
        key = cv2.waitKey(1) & 0xFF
        if key == ord('q'):
            self._endVideo = True

    def multi_display( self, first_image, second_image, window_name="multi_cam" ):
        resize_frame_A = cv2.resize( first_image, (int(self.display_width/2), self.display_height))
        resize_frame_B = cv2.resize( second_image, (int(self.display_width/2), self.display_height))
        self.display_image  = numpy.concatenate((resize_frame_A, resize_frame_B), axis=1)
        cv2.imshow( window_name, self.display_image )
        key = cv2.waitKey(1) & 0xFF
        if key == ord('q'):
            self._endVideo = True

    def FourCam_display( self, first_image, second_image, third_image, fourth_image, window_name="Four Camera" ):
        resize_frame_A = cv2.resize( first_image, (int(self.display_width/2), int(self.display_height/2)))
        resize_frame_B = cv2.resize( second_image, (int(self.display_width/2), int(self.display_height/2)))
        resize_frame_C = cv2.resize( third_image, (int(self.display_width/2), int(self.display_height/2)))
        resize_frame_D = cv2.resize( fourth_image, (int(self.display_width/2), int(self.display_height/2)))

        resize_frame_E = numpy.concatenate((resize_frame_A, resize_frame_B), axis=1)
        resize_frame_F = numpy.concatenate((resize_frame_C, resize_frame_D), axis=1)

        self.display_image = numpy.concatenate((resize_frame_E, resize_frame_F), axis=0)

        cv2.imshow( window_name, self.display_image )
        key = cv2.waitKey(1) & 0xFF
        if key == ord('q'):
            self._endVideo = True

    def drawLine( self, img, x_coordinate ):
        first_coordinate  = (int(x_coordinate), 0)
        second_coordinate = (int(x_coordinate), int(img.shape[0]))
        cv2.line( img, first_coordinate, second_coordinate, (0, 0, 255), thickness=2 )
        return img

    def writeToVideo( self ):
        self.out_video.write( self.display_image )

    def releaseVideo( self ):
        self.out_video.release()

class DetectionDraw( DrawOutput ):

    def __init__( self, bbox_color ):
        super().__init__( bbox_color )

    def draw( self, input_image, detection_results ):
        for detection in detection_results:
            cv2.rectangle( input_image, detection.x1y1, detection.x2y2, self.bboxColor, self.lineThickness )

        self.display( input_image )

class TrackingDraw( DrawOutput ):

    def textPosition( self, x1y1 ):
        x1, y1 = x1y1
        return (x1, y1 - 20)

    def __init__( self, bbox_color ):
        super().__init__( bbox_color )

    def draw( self, input_image, tracking_results ):
        for tracking in tracking_results:
            cv2.rectangle( input_image, tracking.x1y1, tracking.x2y2, self.bboxColor, self.lineThickness )
            id_position = self.textPosition( tracking.x1y1 )
            cv2.putText  ( input_image, str(tracking.track_id) , id_position, self.fontType, self.fontScale, self.bboxColor, self.lineThickness )

        self.display( input_image )

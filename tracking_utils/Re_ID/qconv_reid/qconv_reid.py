# QACONv
from .reid.models import resmap
from .reid.models.qaconv import QAConv
import os
from .reid.utils.serialization import load_checkpoint
from .reid.evaluators import Evaluator
from PIL import Image
from .reid.utils.data import transforms as T
from torchvision.transforms import InterpolationMode
from torch.backends import cudnn
import torch
from torch import nn
import numpy as np
import glob
import cv2
from .reid import datasets
from torch.utils.data import DataLoader
from .reid.utils.data.preprocessor import Preprocessor
from .reid.loss.triplet_loss import TripletLoss

class QConv_Model:
    def __init__(self, ibn_type='b', final_layer='layer3', arch='resnet50', neck=128, weights_file="./weights/checkpoint.pth.tar"):
        self.ibn_type = ibn_type
        self.arch = arch
        self.final_layer = final_layer
        self.neck = neck
        self.model = resmap.create(self.arch, ibn_type=self.ibn_type, final_layer=self.final_layer, neck=self.neck).cuda()
        self.num_features = self.model.num_features
        self.height = 192  #input image height
        self.width  = 64  #input image width

        feamap_factor = {'layer2': 8, 'layer3': 16, 'layer4': 32}
        input_height = self.height // feamap_factor[self.final_layer]
        input_width = self.width // feamap_factor[self.final_layer]
        self.matcher = QAConv(self.num_features, input_height, input_width).cuda()
        self.checkpoint = load_checkpoint(weights_file)
        self.criterion = TripletLoss(self.matcher, 16).cuda()

        # Optimizer
        self.base_param_ids = set(map(id, self.model.base.parameters()))
        self.new_params = [p for p in self.model.parameters() if
                          id(p) not in self.base_param_ids]
        self.param_groups = [
                {'params': self.model.base.parameters(), 'lr': 0.1 * 0.005},
                {'params': self.new_params, 'lr': 0.005},
                {'params': self.matcher.parameters(), 'lr': 0.005}]

        self.optimizer = torch.optim.SGD(self.param_groups)

        self.model.load_state_dict(self.checkpoint['model'])
        self.criterion.load_state_dict(self.checkpoint['criterion'])
        self.optimizer.load_state_dict(self.checkpoint['optim'])
        self.start_epoch = self.checkpoint['epoch']
        self.base_loss = self.checkpoint['base_loss']
        self.final_epochs = self.checkpoint['final_epochs']
        self.lr_stepped = self.checkpoint['lr_stepped']
        self.model = nn.DataParallel(self.model).cuda()
        self.model = self.model.cuda().eval()
        self.evaluator = Evaluator(self.model)
        self.transform = T.Compose([
            T.Resize((self.height, self.width), interpolation=3),
            T.ToTensor()
        ])

    def process_img(self, cv_image):
        pil_img = cv2.cvtColor(cv_image, cv2.COLOR_BGR2RGB)
        pil_img = Image.fromarray(pil_img)
        input_img = self.transform(pil_img)
        input_img = torch.reshape(input_img, (-1, 3, self.height, self.width))
        return input_img

    def extract_cnn_feature(self, cv2_image_data):
        #x1, y1, x2, y2 = bbox_data[0]
        #inputs = cv2_image_data[int(y1):int(y2), int(x1):int(x2)]
        inputs = self.process_img(cv2_image_data)
        with torch.no_grad():
            outputs = self.model(inputs)
        outputs = outputs.cpu()
        return outputs

    def pairwise_distance(self, query_feature, gallery_feature, gal_batch_size=1, prob_batch_size=1, normalised_score=None):
        with torch.no_grad():
            num_gals = gallery_feature.size(0)
            num_probs = query_feature.size(0)
            score = torch.zeros(num_probs, num_gals, device=query_feature.device)
            self.matcher.eval()
            for i in range(0, num_probs, prob_batch_size):
                j = min(i + prob_batch_size, num_probs)
                self.matcher.make_kernel(query_feature[i: j, :, :, :].cuda())
                for k in range(0, num_gals, gal_batch_size):
                    k2 = min(k + gal_batch_size, num_gals)
                    score[i: j, k: k2] = self.matcher(gallery_feature[k: k2, :, :, :].cuda())
            # scale matching scores to make them visually more recognizable
            score = torch.sigmoid(score / 10)

        output_score = (1. - score).cpu()
        result_score = output_score.item()

        return result_score

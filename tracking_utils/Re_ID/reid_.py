from .cosine_metric.reid_model import Extractor
from .trans_reid.infer_utils.trans_model import TransModel
from .qconv_reid.qconv_reid import QConv_Model
import numpy

class ReID:

    @property
    def weightsDir( self ):
        return self.__weights_dir

    @property
    def reid_thresh( self ):
        return self.__reid_thresh

    @property
    def reid_max_thresh( self ):
        return self.__reid_max_thresh

    @property
    def highestValue( self ):
        return self.__reid_highest

    @property
    def lowestValue( self ):
        return self.__reid_lowest

    def __init__( self, weights_dir, reid_thresh, reid_max_thresh, reid_highest, reid_lowest ):
        self.__weights_dir     = weights_dir
        self.__reid_thresh     = reid_thresh
        self.__reid_max_thresh = reid_max_thresh
        self.__reid_highest    = reid_highest
        self.__reid_lowest     = reid_lowest

    def getFeatures( self, img_data ):
        pass

class CosineMetric_ReID( ReID ):

    def __init__( self, weights_dir ):
        self.__reid_thresh     = 0.025
        self.__reid_max_thresh = 0.08
        self.__reid_highest    = 1.0
        self.__reid_lowest     = 0.01
        self.normalised_score  = 0.01
        super().__init__( weights_dir, self.__reid_thresh, self.__reid_max_thresh, self.__reid_highest, self.__reid_lowest )
        self.__extractor       = Extractor(f"{self.weightsDir}", use_cuda=True)

    def getFeatures( self, query_image ):
        output_feature = self.__extractor(query_image)
        return output_feature

    def similarityScore( self, query_feature, gallery_feature, normalised_score=None ):
        similarity_ = 1. - numpy.dot(query_feature, gallery_feature.T)[0][0] - self.normalised_score
        similarity_ = self.__extractor.getScore( similarity_, normalised_score )

        return similarity_

class Trans_ReID( ReID ):

    def __init__( self, weights_dir ):
        self.__reid_thresh           = 600
        self.__reid_max_thresh       = 900
        self.__reid_highest          = 1000
        self.__reid_lowest           = 10
        super().__init__( weights_dir, self.__reid_thresh, self.__reid_max_thresh, self.__reid_highest, self.__reid_lowest )
        self.__transReid_config_file = "tracking_utils/Re_ID/trans_reid/config/vit_transreid.yml"
        self.__reid_model            = TransModel( self.__transReid_config_file, self.weightsDir )

    def getFeatures( self, query_image ):
        output_feature = self.__reid_model.getFeature( query_image[0] )
        return output_feature

    def similarityScore( self, query_feature, gallery_feature ):
        similarity_ = self.__reid_model.euclidean_distance( query_feature, gallery_feature )
        return similarity_[0][0]


class QConv_Reid( ReID ):

    def __init__( self, weights_dir ):
        self.__reid_thresh     = 0.5
        self.__reid_max_thresh = 0.9
        self.__reid_highest    = 0.9
        self.__reid_lowest     = 0.01
        super().__init__( weights_dir, self.__reid_thresh, self.__reid_max_thresh, self.__reid_highest, self.__reid_lowest )
        self.__reid_model      = QConv_Model( weights_file=self.weightsDir )

    def getFeatures( self, query_image ):
        output_feature = self.__reid_model.extract_cnn_feature( query_image[0] )
        return output_feature

    def similarityScore( self, query_feature, gallery_feature, normalised_score=None ):
        similarity_ = self.__reid_model.pairwise_distance( query_feature, gallery_feature, normalised_score=normalised_score )
        return similarity_

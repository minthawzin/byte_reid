from ..config import cfg
from ..model import make_model
import os, torch, cv2, warnings
from PIL import Image
import torchvision.transforms as T
warnings.filterwarnings("ignore", category=UserWarning)

class TransModel:

    @property
    def config_file( self ):
        return self.__config_file

    @property
    def weights_file( self ):
        return self.__weights_file

    @property
    def device( self ):
        return self.__device

    @property
    def model( self ):
        return self.__model

    @property
    def inputSize( self ):
        return self.__input_size

    def __init__( self, config_file, weights_file ):
        self.__config_file  = config_file
        self.__weights_file = weights_file
        self.__device = "cuda"
        self.__input_size = [256, 128]
        self.transforms = T.Compose([
            T.Resize( self.inputSize ),
            T.ToTensor(),
            T.Normalize(mean= [0.5, 0.5, 0.5], std= [0.5, 0.5, 0.5] )
        ])
        self.setup_cfg()

    def setup_cfg( self ):
        """
            Setup code referenced from TransREID test.py
        """
        cfg.merge_from_file(self.config_file)
        cfg.freeze()

        output_dir = cfg.OUTPUT_DIR
        if output_dir and not os.path.exists(output_dir):
            os.makedirs(output_dir)

        #logger = setup_logger("transreid", output_dir, if_train=False)

        if self.config_file != "":
            #logger.info("Loaded configuration file {}".format(self.config_file))
            with open(self.config_file, 'r') as cf:
                config_str = "\n" + cf.read()
                #logger.info(config_str)
        #logger.info("Running with config:\n{}".format(cfg))

        os.environ['CUDA_VISIBLE_DEVICES'] = cfg.MODEL.DEVICE_ID

        #train_loader, train_loader_normal, val_loader, num_query, num_classes, camera_num, view_num = make_dataloader(cfg)
        self.__model = make_model(cfg, num_class=751, camera_num=6, view_num = 1)
        self.__model.load_param(self.weights_file)
        self.__model.to(self.device).eval()

    def euclidean_distance(self, qf, gf):
        m = qf.shape[0]
        n = gf.shape[0]
        dist_mat = torch.pow(qf, 2).sum(dim=1, keepdim=True).expand(m, n) + \
                   torch.pow(gf, 2).sum(dim=1, keepdim=True).expand(n, m).t()
        dist_mat.addmm_(1, -2, qf, gf.t())
        return dist_mat.cpu().numpy()

    def getFeature( self, query_image_cv2 ):
        pil_query = self.cv2PIL( query_image_cv2 )
        pil_preprocess_ = self.transforms( pil_query )
        pil_preprocess_ = pil_preprocess_.unsqueeze(0)
        with torch.no_grad():
            query_image = pil_preprocess_.to( self.device )
            query_feature = self.model (query_image, 1,1 )

        return query_feature

    def cv2PIL( self, cv2_image ):
        rgb_img = cv2.cvtColor( cv2_image, cv2.COLOR_BGR2RGB)
        pil_img = Image.fromarray( rgb_img)
        return pil_img

    def PIL2cv( self, pil_image ):
        rgb_image = numpy.asarray( pil_image )
        cv2_image = cv2.cvtColor(rgb_image, cv2.COLOR_RGB2BGR)
        return cv2_image

    def inference( self, query_image, gallery_image ):
        with torch.no_grad():

            query_image   = query_image.to( self.device )
            gallery_image = gallery_image.to( self.device )

            query_feature   = self.model( query_image ,1 ,1 )
            gallery_feature = self.model( gallery_image ,1 ,1)

            similarity_score = self.euclidean_distance( query_feature, gallery_feature )

        return similarity_score

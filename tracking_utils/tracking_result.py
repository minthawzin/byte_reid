class TrackingResult:

    @property
    def personBBoxList( self ):
        return self.__person_bbox_list

    @property
    def personScoreList( self ):
        return self.__person_scores_list

    @property
    def bicycleBboxList( self ):
        return self.__bicycle_bbox_list

    @property
    def busBboxList( self ):
        return self.__bus_bbox_list

    @property
    def busPresent( self ):
        if len( self.busBboxList ) > 0:
            return True
        else:
            return False

    def __init__( self, detection_results ):
        self.__person_bbox_list   = []
        self.__person_scores_list = []
        self.__bicycle_bbox_list  = []
        self.__bus_bbox_list      = []
        for detection in detection_results:
            if detection.class_name == 'person':
                self.__person_bbox_list.append(detection.bbox)
                self.__person_scores_list.append(detection.confidence)

            elif detection.class_name == 'bicycle':
                self.__bicycle_bbox_list.append(detection.bbox)

            elif detection.class_name == 'bus':
                self.__bus_bbox_list.append(detection.bbox)

            else:
                pass

from file_utils import yaml_parser
from tracking_utils.ByteTrack.byte_tracker import BYTETracker
from tracking_utils.tracking_result import TrackingResult
from tracking_utils.Re_ID import reid_
import torch, os, json

class Tracker:

    @property
    def sourceVideoFPS( self ):
        return self.__videoFPS

    @property
    def sourceVideoName( self ):
        return self.__videoName

    @property
    def cosine_reid( self ):
        return self.__cosine_weight_file

    @property
    def trans_reid( self ):
        return self.__transReid_weights_file

    @property
    def qconv_reid( self ):
        return self.__qconv_weights_file

    @property
    def getTrackResults( self ):
        return self.__updated_tracks

    @property
    def maskData( self ):
        return self.__mask_data

    def __init__( self, bytetrack_config, movieLoader, mask_data=None, starting_id = 1, interested_direction=None ):
        self.__track_settings          = yaml_parser.YamlParser( config_file=bytetrack_config )
        self.__videoFPS                = movieLoader.FPS
        self.__videoName               = movieLoader.movieName
        #self.__cosine_weight_file      = "tracking_utils/Re_ID/cosine_metric/weights/deepsort.t7"
        self.__transReid_weights_file  = "tracking_utils/Re_ID/trans_reid/weights/vit_transreid_market.pth"
        # self.__qconv_weights_file      = "tracking_utils/Re_ID/qconv_reid/weights/checkpoint.pth.tar"
        self.__mask_data               = mask_data
        self.unique_id                 = starting_id
        self.interested_direction      = interested_direction
        self.setupSystem()

    def __str__( self ):
        return str( self.__track_settings )

    def setupSystem( self ):
        #self.__reid_model = reid_.CosineMetric_ReID( self.cosine_reid )
        self.__reid_model = reid_.Trans_ReID( self.trans_reid )
        #self.__reid_model  = reid_.QConv_Reid( self.qconv_reid )
        self.tracking_system = BYTETracker( self.__track_settings, self.sourceVideoFPS, self.__reid_model, self.maskData, self.interested_direction )
        self.output_json  = []

        self.output_json_dir = f"output/tracking_results/{self.sourceVideoName}.json"

    def updateFrame( self, detection_results, movieLoader ):
        with torch.no_grad():
            tracking_result = TrackingResult( detection_results )
            self.__updated_tracks  = self.tracking_system.trackFrame( tracking_result, movieLoader.currentFrame, movieLoader.frameId )

    def outputJSON( self, camera_index ):
        unique_id = self.unique_id
        timeline_list = []

        unique_id_list = []
        unique_byte_list = []
        for track in self.tracking_system.all_stracks:
            unique_id_list.append(track.track_id)
            unique_byte_list.append(track.bytetrack_id)

        for index, track in enumerate(self.tracking_system.all_stracks):
            track.checkBicycle()
            frames_list = []
            # add first frame
            if unique_id_list[index] == unique_byte_list[index]:
                byte_id = unique_id
            else:
                byte_id = unique_id + unique_byte_list[index]
            if track.bus_list[0] != 2:
                first_dict = {
                    "bbox": list(track.bbox_list[0]),
                    "category": track.class_name,
                    "frame": track.frame_list[0]-1,
                    "id": f"{camera_index}_{unique_id}",
                    "bid": byte_id,
                    "area": track.area_list[0],
                    "bus": track.bus_list[0],
                    "bytetrack_IOU": track.byte_iou_list[0],
                    "detect_confidence": track.det_conf_list[0],
                    "tracking_confidence": track.track_conf_list[0],
                }
            else:
                first_dict = {
                    "bbox": list(track.bbox_list[0]),
                    "category": track.class_name,
                    "frame": track.frame_list[0]-1,
                    "id": f"{camera_index}_{unique_id}",
                    "bid": byte_id,
                    "area": track.area_list[0],
                    "bytetrack_IOU": track.byte_iou_list[0],
                    "detect_confidence": track.det_conf_list[0],
                    "tracking_confidence": track.track_conf_list[0],
                }
            frames_list.append(first_dict)
            for i in range(len(track.bbox_list)):
                if track.bus_list[i] != 2:
                    frame_dict = {
                        "bbox": list(track.bbox_list[i]),
                        "category": track.class_name,
                        "frame": track.frame_list[i],
                        "id": f"{camera_index}_{unique_id}",
                        "bid": byte_id,
                        "area": track.area_list[i],
                        "bus": track.bus_list[i],
                        "bytetrack_IOU": track.byte_iou_list[i],
                        "detect_confidence": track.det_conf_list[i],
                        "tracking_confidence": track.track_conf_list[i],
                    }
                else:
                    frame_dict = {
                        "bbox": list(track.bbox_list[i]),
                        "category": track.class_name,
                        "frame": track.frame_list[i],
                        "id": f"{camera_index}_{unique_id}",
                        "bid": byte_id,
                        "area": track.area_list[i],
                        "bytetrack_IOU": track.byte_iou_list[i],
                        "detect_confidence": track.det_conf_list[i],
                        "tracking_confidence": track.track_conf_list[i],
                    }
                frames_list.append(frame_dict)

            timeline_dict = {
                "category": track.class_name,
                "id": f"{camera_index}_{unique_id}",
                "bid": byte_id,
                "bicycle": track.bicycle,
                "frames": frames_list
            }
            timeline_list.append(timeline_dict)
            unique_id += 1

        output_json = {
            "timelines": timeline_list
        }

        os.makedirs(os.path.dirname(self.output_json_dir), exist_ok=True)
        with open(f"{self.output_json_dir}", 'w') as jsonFile:
            json.dump( output_json, jsonFile, indent=4, ensure_ascii=False )

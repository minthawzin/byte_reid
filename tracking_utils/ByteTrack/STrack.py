from .basetrack import BaseTrack, TrackState
from .kalman_filter import KalmanFilter
import numpy

class STrack(BaseTrack):
    shared_kalman = KalmanFilter()
    def __init__(self, tlwh=None, score=None, class_name=None, feature_len = None ):

        # wait activate
        self._tlwh = numpy.asarray(tlwh, dtype=numpy.float)
        self.kalman_filter = None
        self.mean, self.covariance = None, None
        self.is_activated   = False
        self.features       = []
        self.bbox_list      = []
        self.track_conf_list= []
        self.det_conf_list  = []
        self.frame_list     = []
        self.area_list      = []
        self.bus_list       = []
        self.byte_iou_list  = []
        self.score = score
        self.tracklet_len = 0
        self.class_name = class_name
        self.bicycle_frame = 0
        self.bicycle = 0
        self.feature_len = feature_len
        # 2 sec 60 frames
        self.direction_update_interval = 20
        self.compared_direction_coordinate = None
        self.move_direction = None

    def predict(self):
        mean_state = self.mean.copy()
        if self.state != TrackState.Tracked:
            mean_state[7] = 0
        self.mean, self.covariance = self.kalman_filter.predict(mean_state, self.covariance)

    @staticmethod
    def multi_predict(stracks):
        if len(stracks) > 0:
            multi_mean = numpy.asarray([st.mean.copy() for st in stracks])
            multi_covariance = numpy.asarray([st.covariance for st in stracks])
            for i, st in enumerate(stracks):
                if st.state != TrackState.Tracked:
                    multi_mean[i][7] = 0
            multi_mean, multi_covariance = STrack.shared_kalman.multi_predict(multi_mean, multi_covariance)
            for i, (mean, cov) in enumerate(zip(multi_mean, multi_covariance)):
                stracks[i].mean = mean
                stracks[i].covariance = cov

    def activate(self, kalman_filter, frame_id, feature_data=None, bbox_data=None ):
        """Start a new tracklet"""
        self.kalman_filter = kalman_filter
        self.track_id = self.next_id
        self.bytetrack_id = self.track_id
        self.mean, self.covariance = self.kalman_filter.initiate(self.tlwh_to_xyah(self._tlwh))
        if feature_data is not None:
            if len(self.features) == self.feature_len:
                self.features.pop(0)
            self.features.append(feature_data)
        self.tracklet_len = 0
        self.state = TrackState.Tracked
        self.bbox  = bbox_data
        #self.is_activated = True
        self.frame_id = frame_id
        #self.frame_list.append(self.frame_id)
        self.start_frame = frame_id
        # x1
        self.compared_direction_coordinate = self.bbox[0][0]

    def re_activate(self, new_track, frame_id, new_id=False, feature_data=None, id_assign=None, track_score=None, area_flag = None, bus_flag=None, previous_id=None, cost_ = None):
        self.mean, self.covariance = self.kalman_filter.update(
            self.mean, self.covariance, self.tlwh_to_xyah(new_track.tlwh)
        )
        self.tracklet_len = self.tracklet_len + 1
        self.state = TrackState.Tracked
        self.is_activated = True
        self.frame_id = frame_id
        self.frame_list.append(self.frame_id)
        self.area_list.append(area_flag)
        self.byte_iou_list.append(cost_)
        if bus_flag is None:
            # minor fix
            bus_flag = 2
        self.bus_list.append(bus_flag)
        if feature_data is not None:
            if len(self.features) == self.feature_len:
                self.features.pop(0)
            self.bbox = new_track.tlbr
            self.features.append(feature_data)
            self.bbox_list.append(self.bbox)
        if new_id:
            self.features = []
            self.features.append(feature_data)
            self.track_id = id_assign
        self.score = new_track.score
        self.det_conf_list.append(self.score)
        if track_score is None:
            track_score = self.score
        self.track_conf_list.append(track_score)

    def update(self, new_track, frame_id, feature_data=None, track_score=None,area_flag =None,  bus_flag=None, previous_id=None, cost_=None):
        """
        Update a matched track
        :type new_track: STrack
        :type frame_id: int
        :type update_feature: bool
        :return:
        """
        self.frame_id = frame_id
        #self.frame_list.append(self.frame_id)
        new_tlwh = new_track.tlwh
        new_tlbr = new_track.tlbr
        #self.bbox_list.append(self.bbox)
        self.mean, self.covariance = self.kalman_filter.update(
            self.mean, self.covariance, self.tlwh_to_xyah(new_tlwh))
        self.state = TrackState.Tracked
        self.is_activated = True
        if self.tracklet_len == 0:
            self.first_bbox = new_tlbr
        if feature_data is not None:
            self.bbox = new_tlbr
            if len(self.features) == self.feature_len:
                self.features.pop(0)
            self.frame_list.append(self.frame_id)
            self.tracklet_len += 1
            self.features.append(feature_data)
            self.bbox = new_tlbr
            self.bbox_list.append(self.bbox)
            self.score = new_track.score
            self.det_conf_list.append(self.score)
            if track_score is None:
                track_score = self.score
            self.track_conf_list.append(track_score)
            self.area_list.append(area_flag)
            if bus_flag is None:
                # minor fix
                bus_flag = 2
            self.bus_list.append(bus_flag)
            self.byte_iou_list.append(cost_)

        # update direction
        if self.tracklet_len % self.direction_update_interval == 0:
            self.computeDirection()

    def computeDirection( self ):
        if self.compared_direction_coordinate == None:
            return

        x_difference = self.bbox[0] - self.compared_direction_coordinate
        if x_difference >= 0:
            self.move_direction = "RIGHT"
        elif x_difference < 0:
            self.move_direction = "LEFT"
        else:
            pass


    def bicycle_flag(self):
        self.bicycle_frame += 1

    def checkBicycle(self):
        if self.bicycle_frame > int(0.5 * self.tracklet_len):
            self.bicycle = 1

    @property
    def x1y1( self ):
        return tuple(map(int, self.bbox[:2]))

    @property
    def x2y2( self ):
        return tuple(map(int, self.bbox[2:4]))

    @property
    def centerX( self ):
        return int(self.bbox[0] + (int(self.bbox[2] - self.bbox[0])/2))

    @property
    # @jit(nopython=True)
    def tlwh(self):
        """Get current position in bounding box format `(top left x, top left y,
                width, height)`.
        """
        if self.mean is None:
            return self._tlwh.copy()
        ret = self.mean[:4].copy()
        ret[2] *= ret[3]
        ret[:2] -= ret[2:] / 2
        return ret

    @property
    # @jit(nopython=True)
    def tlbr(self):
        """Convert bounding box to format `(min x, min y, max x, max y)`, i.e.,
        `(top left, bottom right)`.
        """
        ret = self.tlwh.copy()
        ret[2:] += ret[:2]
        return ret

    @staticmethod
    # @jit(nopython=True)
    def tlwh_to_xyah(tlwh):
        """Convert bounding box to format `(center x, center y, aspect ratio,
        height)`, where the aspect ratio is `width / height`.
        """
        ret = numpy.asarray(tlwh).copy()
        ret[:2] += ret[2:] / 2
        ret[2] /= ret[3]
        return ret

    def to_xyah(self):
        return self.tlwh_to_xyah(self.tlwh)

    @staticmethod
    # @jit(nopython=True)
    def tlbr_to_tlwh(tlbr):
        ret = numpy.asarray(tlbr).copy()
        ret[2:] -= ret[:2]
        return ret

    @staticmethod
    # @jit(nopython=True)
    def tlwh_to_tlbr(tlwh):
        ret = numpy.asarray(tlwh).copy()
        ret[2:] += ret[:2]
        return ret

    def __repr__(self):
        return 'OT_{}_({}-{})'.format(self.track_id, self.start_frame, self.end_frame)

import numpy as np
from collections import deque
import os
import os.path as osp
import copy
import torch
import torch.nn.functional as F
import logging
from .kalman_filter import KalmanFilter
from . import matching
from .basetrack import BaseTrack, TrackState
from .STrack import STrack
import cv2
import json
import copy
import math
import torchvision.ops.boxes as bops
from typing import List
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon

class BYTETracker(object):
    def __init__( self, config_args, frame_rate, reid_model, mask_data, interested_direction=None ):
        self.tracked_stracks = []  # type: list[STrack]
        self.lost_stracks    = []  # type: list[STrack]
        self.removed_stracks = []  # type: list[STrack]
        self.all_stracks     = []
        self.kalman_filter   = KalmanFilter()
        self.reid_model      = reid_model
        self.mask_data       = mask_data
        self.interested_direction = interested_direction
        self.setupCFG( config_args )

    def setupCFG( self, args ):
        self.det_thresh       = args.track_thresh
        self.max_time_lost    = args.max_time_lost
        self.timeline_length  = args.timeline_length
        self.compare_distance = args.compare_distance
        self.max_distance_pts = args.max_distance_pts
        self.match_thresh     = args.match_thresh
        self.new_tracklet_len = args.new_tracklet_len
        reset_id = STrack()
        reset_id.reset_id()

    def _get_features(self, bbox_xyxy, ori_img):
        im_crops = []
        for box in bbox_xyxy:
            x1, y1, x2, y2 = box
            im = ori_img[int(y1):int(y2), int(x1):int(x2)]
            im_crops.append(im)
        if im_crops:
            features = self.reid_model.getFeatures(im_crops)
        else:
            features = np.array([])
        return features

    def trackFrame( self, tracking_result, img_data, frame_id ):
        self.frame_id = frame_id
        activated_starcks = []
        refind_stracks = []
        lost_stracks = []
        removed_stracks = []

        bus_flag = 0
        scores = np.array(tracking_result.personScoreList)
        bboxes = np.array(tracking_result.personBBoxList)

        #print( self.getArea() )

        remain_inds = scores > self.det_thresh
        inds_low = scores > self.det_thresh
        inds_high = scores < self.det_thresh

        inds_second = np.logical_and(inds_low, inds_high)
        dets_second = bboxes[inds_second]
        dets = bboxes[remain_inds]
        scores_keep = scores[remain_inds]
        scores_second = scores[inds_second]

        if len(dets) > 0:
            '''Detections'''
            detections = [STrack(STrack.tlbr_to_tlwh(tlbr), s, "person", self.timeline_length) for
                          (tlbr, s) in zip(dets, scores_keep)]
        else:
            detections = []

        ''' Add newly detected tracklets to tracked_stracks'''
        unconfirmed = []
        tracked_stracks = []  # type: list[STrack]
        for track in self.tracked_stracks:
            if not track.is_activated:
                unconfirmed.append(track)
            else:
                tracked_stracks.append(track)

        ''' Step 2: First association, with high score detection boxes'''
        strack_pool = joint_stracks(tracked_stracks, self.lost_stracks)
        # Predict the current location with KF
        STrack.multi_predict(strack_pool)
        dists = matching.iou_distance(strack_pool, detections)
        dists = matching.fuse_score(dists, detections)
        if len(tracking_result.personBBoxList) == 0:
            for track in self.tracked_stracks:
                track.mark_lost()
                self.lost_stracks.append(track)

        matches, u_track, u_detection = matching.linear_assignment(dists, thresh=self.match_thresh)

        for itracked, idet, cost_ in matches:
            track = strack_pool[int(itracked)]
            if track in self.lost_stracks:
                self.lost_stracks.remove(track)

        for itracked, idet, cost_ in matches:
            track = strack_pool[int(itracked)]
            det = detections[int(idet)]
            bbox_data = [det.tlbr.tolist()]
            feature_data = self._get_features(bbox_data, img_data)
            area_flag = self.getArea( bbox_data )
            bus_flag  = self.getBus( area_flag, tracking_result.busPresent )

            distance = 0
            count = 0
            # if track id is already present, skip
            # new id initialise period

            # if new track are assigned, look inside lost stracks
            # Re-id step comparing algo
            if track.tracklet_len < self.new_tracklet_len:
                is_old_id = False
                least_distance_to_bbox = self.reid_model.highestValue
                for old_track in self.lost_stracks:
                    similarity_score = self.reIdentify(feature_data, self.timeline_length, old_track.features, self.compare_distance)
                    # similarity comparison, if 0.03 > then similar, else not similar
                    if len(old_track.bbox_list) == 0:
                        continue
                    if old_track.tracklet_len < self.new_tracklet_len:
                        continue


                    line_dist = self.distance_between_two_points(bbox_data[0], old_track.bbox_list[-1])
                    track_score = self.calc_track_score(similarity_score)
                    if similarity_score < 300:
                        # check if its shorter than the shortest distance
                        if similarity_score < least_distance_to_bbox:
                            least_distance_to_bbox = similarity_score
                            reid_score = similarity_score
                            matched_track = old_track
                            is_old_id = True
                        continue

                if is_old_id == True:
                    #self.reid_count += 1
                    track_score = self.calc_track_score(reid_score)
                    matched_track.re_activate(det, self.frame_id, new_id=False, feature_data=feature_data, track_score=track_score, area_flag = area_flag, bus_flag= bus_flag, previous_id = track.track_id, cost_=cost_)
                    refind_stracks.append(matched_track)
                    self.lost_stracks.remove(matched_track)
                else:
                    similarity_score = self.self_Identify(feature_data, self.timeline_length, track.features, self.compare_distance)
                    track_score = self.calc_track_score(similarity_score)
                    if track.state == TrackState.Tracked:
                        track.update(det, self.frame_id, feature_data=feature_data, track_score=track_score, area_flag = area_flag, bus_flag=bus_flag, cost_=cost_)
                        activated_starcks.append(track)
                    else:
                        track.re_activate(det, self.frame_id, new_id=False, feature_data=feature_data, track_score=track_score, area_flag = area_flag, bus_flag= bus_flag, cost_=cost_)
                        refind_stracks.append(track)

            else:
                # If not new tracks, just follow bytetrack
                similarity_score = self.self_Identify(feature_data, self.timeline_length, track.features, self.compare_distance)
                track_score = self.calc_track_score(similarity_score)
                if track.state == TrackState.Tracked:
                    track.update(det, self.frame_id, feature_data=feature_data,area_flag = area_flag, bus_flag=bus_flag, track_score=track_score, cost_=cost_)
                    activated_starcks.append(track)
                else:
                    track.re_activate(det, self.frame_id, new_id=False, feature_data=feature_data, track_score=track_score, area_flag = area_flag, bus_flag = bus_flag, cost_=cost_)
                    refind_stracks.append(track)

            for bicycle in tracking_result.bicycleBboxList:
                iou_ = self.computer_overlap(bicycle, bbox_data[0])
                if iou_ > 0.2:
                    track.bicycle_flag()

        ''' Step 3: Second association, with low score detection boxes'''
        # association the untrack to the low score detections
        if len(dets_second) > 0:
            '''Detections'''
            detections_second = [STrack(STrack.tlbr_to_tlwh(tlbr), s, "person") for
                          (tlbr, s) in zip(dets_second, scores_second)]
        else:
            detections_second = []


        r_tracked_stracks = [strack_pool[i] for i in u_track if strack_pool[i].state == TrackState.Tracked]
        dists = matching.iou_distance(r_tracked_stracks, detections_second)
        matches, u_track, u_detection_second = matching.linear_assignment(dists, thresh=self.match_thresh)
        for itracked, idet, cost_ in matches:
            track = r_tracked_stracks[int(itracked)]
            det = detections_second[int(idet)]
            bbox_data = [det.tlbr.tolist()]
            feature_data = self._get_features(bbox_data, img_data)
            if track.state == TrackState.Tracked:
                track.update(det, self.frame_id)
                activated_starcks.append(track)
            else:
                track.re_activate(det, self.frame_id, new_id=False)
                refind_stracks.append(track)

        for it in u_track:
            track = r_tracked_stracks[it]
            track.mark_lost()
            lost_stracks.append(track)

        '''Deal with unconfirmed tracks, usually tracks with only one beginning frame'''
        detections = [detections[i] for i in u_detection]
        dists = matching.iou_distance(unconfirmed, detections)
        dists = matching.fuse_score(dists, detections)
        matches, u_unconfirmed, u_detection = matching.linear_assignment(dists, thresh=self.match_thresh)
        for itracked, idet, cost_ in matches:
            det = detections[int(idet)]
            bbox_data = [det.tlbr.tolist()]
            feature_data = self._get_features(bbox_data, img_data)
            unconfirmed[int(itracked)].update(det, self.frame_id, feature_data=feature_data,area_flag ="NP", cost_= cost_)
            activated_starcks.append(unconfirmed[int(itracked)])

        for it in u_unconfirmed:
            track = unconfirmed[it]
            track.mark_removed()
            removed_stracks.append(track)

        """ Step 4: Init new stracks"""
        for inew in u_detection:
            track = detections[inew]
            bbox_data = [track.tlbr.tolist()]
            det_feature = self._get_features(bbox_data, img_data)
            #track.activate(self.kalman_filter, self.frame_id, feature_data=det_feature)
            track.activate(self.kalman_filter, self.frame_id, feature_data=det_feature, bbox_data=bbox_data )
            activated_starcks.append(track)

        """ Step 5: Update state"""
        for track in self.lost_stracks:
            if self.frame_id - track.end_frame > self.max_time_lost:
                track.mark_removed()
                track.features = []
                removed_stracks.append(track)

        # print('Ramained match {} s'.format(t4-t3))

        self.tracked_stracks = [t for t in self.tracked_stracks if t.state == TrackState.Tracked]
        self.tracked_stracks = joint_stracks(self.tracked_stracks, activated_starcks)
        self.tracked_stracks = joint_stracks(self.tracked_stracks, refind_stracks)
        self.lost_stracks = sub_stracks(self.lost_stracks, self.tracked_stracks)
        self.lost_stracks.extend(lost_stracks)
        self.lost_stracks = sub_stracks(self.lost_stracks, self.removed_stracks)
        self.removed_stracks.extend(removed_stracks)
        #self.tracked_stracks, self.lost_stracks = remove_duplicate_stracks(self.tracked_stracks, self.lost_stracks)
        # get scores of lost tracks
        # join all stracks
        all_stracks = joint_stracks(self.tracked_stracks, self.lost_stracks)
        all_stracks = joint_stracks(all_stracks, self.removed_stracks)
        all_stracks = sorted(all_stracks, key=lambda x: int(str(x).split('_')[1]))
        if self.interested_direction is not None:
            self.all_stracks = [track for track in all_stracks if track.tracklet_len > self.new_tracklet_len and track.move_direction==self.interested_direction]
            curr_stracks = [track for track in self.tracked_stracks if track.tracklet_len > self.new_tracklet_len and track.move_direction==self.interested_direction]
        else:
            self.all_stracks = [track for track in all_stracks if track.tracklet_len > self.new_tracklet_len]
            curr_stracks = [track for track in self.tracked_stracks if track.tracklet_len > self.new_tracklet_len]

        return curr_stracks

    def getArea( self, bbox_data ):
        x_c = ( bbox_data[0][2] + bbox_data[0][0] ) / 2
        y   = bbox_data[0][3]
        area_analysis_point = Point( x_c, y )
        area = "NP"
        if self.mask_data is None:
            return area

        for key, value in self.mask_data.areaAnalysis.items():
            if value.contains( area_analysis_point ):
                area = str(key)

        return area

    def getBus( self, area_flag, bus_present ):
        bus_flag = 2
        if area_flag == 'A2' or area_flag == 'BA2':
            if bus_present == True:
                bus_flag = 1
            else:
                bus_flag = 0

        return bus_flag


    def distance_between_two_points(self, box1, box2):

        x1,y1,x2,y2 = box1
        c_x = (x1 + x2) /2
        c_y = (y1 + y2) /2
        p1 = (c_x,c_y)
        last_bbox = box2
        n_x = (last_bbox[0] + last_bbox[2])/2
        n_y = (last_bbox[3] + last_bbox[1])/2
        p2 = (n_x, n_y)
        distance = math.sqrt(((p1[0]-p2[0])**2)+((p1[1]-p2[1])**2))
        return distance

    def calc_track_score(self, reid_dist: STrack):
        max_score = self.reid_model.highestValue
        min_score = self.reid_model.lowestValue
        if reid_dist > max_score:
            cal_score = self.reid_model.lowestValue
        elif reid_dist < max_score and reid_dist > min_score:
            cal_score = max_score - reid_dist
        else:
            cal_score = max_score - min_score

        cal_score = (cal_score/(max_score - min_score))
        # 1.0 -> 0.99
        if float(round(cal_score,2)) >= 1.00:
            cal_score = 0.99
        # limit to 2 decimal place
        return float(round(cal_score,2))

    def computer_overlap(self, bbox_a, bbox_b):
        box1 = torch.tensor([bbox_a], dtype=torch.float)
        box2 = torch.tensor([bbox_b], dtype=torch.float)
        iou = bops.box_iou(box1, box2)
        iou = iou.to('cpu').numpy()
        iou = iou[0][0]
        return iou


    def reIdentify(self, curr_feature: np.ndarray, timeline_length: int, lost_features: List, compare_distance: int):
        compare_frames = int(timeline_length / compare_distance)
        similarity_score = 0
        frame_count = 0

        is_similar = False
        for index,feature in enumerate(lost_features[-(timeline_length):]):
            if index == 0:
                continue
            if index == self.timeline_length:
                continue
            if (index % compare_distance) == 0:
                similar_ = self.reid_model.similarityScore(curr_feature, feature)
                similarity_score += similar_
                frame_count += 1

        if frame_count == 0:
            # not similar (higher value)
            avg_similarity = self.reid_model.highestValue

        else:
            avg_similarity = similarity_score/frame_count

        return avg_similarity

    def self_Identify(self, curr_feature: np.ndarray, timeline_length: int, lost_features: List, compare_distance: int):
        compare_frames = int(timeline_length / compare_distance)
        similarity_score = 0
        frame_count = 0
        compare_frames = 1
        is_similar = False
        for index,feature in enumerate(lost_features[-(timeline_length):]):
            if index == 0:
                continue
            if index == self.timeline_length:
                continue
            if (index % compare_distance) == 0:
                similar_ = self.reid_model.similarityScore(curr_feature, feature)
                similarity_score += similar_
                frame_count += 1

        if frame_count == 0:
            # not similar (higher value)
            avg_similarity = self.reid_model.highestValue

        else:
            avg_similarity = similarity_score/frame_count

        return avg_similarity

def joint_stracks(tlista, tlistb):
    exists = {}
    res = []
    for t in tlista:
        exists[t.track_id] = 1
        res.append(t)
    for t in tlistb:
        tid = t.track_id
        if not exists.get(tid, 0):
            exists[tid] = 1
            res.append(t)
    return res


def sub_stracks(tlista, tlistb):
    stracks = {}
    for t in tlista:
        stracks[t.track_id] = t
    for t in tlistb:
        tid = t.track_id
        if stracks.get(tid, 0):
            del stracks[tid]
    return list(stracks.values())


def remove_duplicate_stracks(stracksa, stracksb):
    pdist = matching.iou_distance(stracksa, stracksb)
    pairs = np.where(pdist < 0.15)
    dupa, dupb = list(), list()
    for p, q in zip(*pairs):
        timep = stracksa[p].frame_id - stracksa[p].start_frame
        timeq = stracksb[q].frame_id - stracksb[q].start_frame
        if timep > timeq:
            dupb.append(q)
        else:
            dupa.append(p)
    resa = [t for i, t in enumerate(stracksa) if not i in dupa]
    resb = [t for i, t in enumerate(stracksb) if not i in dupb]
    return resa, resb

import numpy as np
from tracking_utils.Re_ID.reid_ import CosineMetric_ReID, QConv_Reid, Trans_ReID
from typing import List
from shapely.geometry import Point

class Track_ID:
    def __init__(self, track_id: int,removal_threshold: int, frame_id: int ):
        self.track_id          = track_id
        self.last_frame        = frame_id
        self.removal_threshold = removal_threshold
        self.near_features     = []
        self.start_frame       = frame_id
        self.feature_storage_  = 3
        self.feature_interval  = 5
        self.tracklet_len      = 0
        self.feature_last_time = 0

    def update(self, frame_id: int):
        self.last_frame = frame_id
        self.tracklet_len += 1

    def storeFeature( self, feature_extractor, crop_img ):
        store_ = False
        if len(self.near_features) == 3:
            pass
        else:
            new_feature = feature_extractor.getFeatures([crop_img.img])
            # 1 beause update is occur first tracklet_len
            if self.tracklet_len > 15 and self.tracklet_len - self.feature_last_time >= self.feature_interval:
                self.near_features.append( new_feature )
                store_ = True
                self.feature_last_time = self.tracklet_len

        return store_

class Tracker:
    def __init__(self, maskData, time_range, camID ):
        self.tracked_ids = []
        self.tracks      = []
        self.reid_list   = []
        self.maskData    = maskData
        self.frame_id    = -1
        self.camID       = camID
        self.feature_extractor = Trans_ReID("tracking_utils/Re_ID/trans_reid/weights/vit_transreid_market.pth")
        #self.feature_extractor  = QConv_Reid("tracking_utils/Re_ID/qconv_reid/weights/pretrained_checkpoint.pth.tar")
        self.removal_threshold = 900 # 30 sec
        # after disappear from camA
        self.time_range = time_range # 3 sec
        self.loadMaskAreas()

    def loadMaskAreas( self ):
        self.near_area = self.maskData.loadSpecificArea('near_features')
        self.far_area  = self.maskData.loadSpecificArea('far_features')

    def update(self, track_id, frame_id, crop_img, evaluate_=None ):
        self.frame_id = frame_id
        for track in self.tracks:
            if track.last_frame + self.removal_threshold < frame_id:
                track.near_features = []
                self.tracks.remove(track)
                self.tracked_ids.remove(track.track_id)

        if track_id not in self.tracked_ids:
            new_track = Track_ID(track_id, self.removal_threshold, frame_id)
            self.tracked_ids.append(track_id)
            self.tracks.append(new_track)
            new_track.update(frame_id)
            if self.near_area.contains(Point( crop_img.midBottomPoint )):
                store_ = new_track.storeFeature( self.feature_extractor, crop_img )
                if evaluate_.evaluate_mode is True and store_ is True:
                    evaluate_.saveTrainImage( crop_img.img, track_id, frame_id, self.camID )
            return new_track.track_id
        else:
            for track in self.tracks:
                if track.track_id == track_id:
                    track.update( frame_id )
                    if self.near_area.contains(Point( crop_img.midBottomPoint )):
                        store_ = track.storeFeature( self.feature_extractor, crop_img )
                        if evaluate_.evaluate_mode is True and store_ is True:
                            # 1 is camID
                            evaluate_.saveTrainImage( crop_img.img, track_id, frame_id, self.camID )
                    return track.track_id

    def compareWithStartCam( self, trackerA, crop_img, frame_id, curr_list, evaluate_=None, start_camera_index="A"):
        most_similar_id = -1
        lowest_similarity_score = self.feature_extractor.highestValue

        if self.near_area.contains( Point(crop_img.midBottomPoint) ):
            feature = self.feature_extractor.getFeatures([crop_img.img])

            for tracks in trackerA.tracks:
                avg_similarity_score = 0
                if frame_id - tracks.last_frame < self.time_range:
                    continue
                if tracks.track_id in self.reid_list:
                    continue

                if len(tracks.near_features) > 0:
                    for camA_features in tracks.near_features:
                        avg_similarity_score += self.feature_extractor.similarityScore( feature, camA_features )

                    avg_similarity_score = avg_similarity_score / len(tracks.near_features)
                    if avg_similarity_score < self.feature_extractor.reid_thresh:
                        if avg_similarity_score < lowest_similarity_score:
                            lowest_similarity_score = avg_similarity_score
                            most_similar_id         = tracks.track_id

        if most_similar_id != -1:
            most_similar_id = f"{start_camera_index}_{most_similar_id}"
            if most_similar_id in curr_list:
                most_similar_id = -1
                lowest_similarity_score = self.feature_extractor.highestValue
            else:
                self.reid_list.append( int(most_similar_id.split("_")[-1]) )
                if evaluate_.evaluate_mode is True:
                    # 2 is camID
                    evaluate_.saveTestImage( crop_img.img, most_similar_id, frame_id, self.camID )

        return most_similar_id, lowest_similarity_score

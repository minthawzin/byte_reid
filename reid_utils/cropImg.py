import cv2, numpy

class cropImg:

    @property
    def width( self ):
        return int( self.bbox[2] - self.bbox[0] )

    @property
    def height( self ):
        return int( self.bbox[3] - self.bbox[1] )

    @property
    def midBottomPoint( self ):
        x_center = int( self.bbox[2] - self.width/2 )
        y_bottom = int( self.bbox[3] )
        return ( x_center, y_bottom )

    @property
    def x1y1( self ):
        return ( self.bbox[0], self.bbox[1])

    @property
    def x2y2( self ):
        return ( self.bbox[2], self.bbox[3])

    @property
    def textPosition( self ):
        return (self.bbox[0], self.bbox[1] - 10)

    @property
    def img( self ):
        img_crop = self.original_img[self.y1:self.y2, self.x1:self.x2]
        img = cv2.resize( img_crop, ( self.resize_width, self.resize_height ), interpolation=cv2.INTER_CUBIC )
        img = self.sharpen( img )
        return img

    def __init__( self, bbox, id, bIOU, original_img ):
        self.resize_width  = 128
        self.resize_height = 256
        self.bbox = [int(i) for i in bbox]
        self.x1, self.y1, self.x2, self.y2 = self.bbox
        self.original_img = original_img
        self.track_id     = id
        self.bIOU         = bIOU

    def save( self, id, frame_number ):
        cv2.imwrite( f"test_folder/{id}_{frame_number}.png", self.img )


    def increase_brightness(self, img, value=30):
        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        h, s, v = cv2.split(hsv)

        lim = 255 - value
        v[v > lim] = 255
        v[v <= lim] += value

        final_hsv = cv2.merge((h, s, v))
        img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
        return img

    def sharpen( self, img ):
        kernel = numpy.array([[0, -1, 0],
                   [-1, 5,-1],
                   [0, -1, 0]])
        image_sharp = cv2.filter2D(src=img, ddepth=-1, kernel=kernel)
        return image_sharp

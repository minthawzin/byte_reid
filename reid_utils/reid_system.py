import tqdm, cv2, copy, json
from drawing_utils import draw_output
from reid_utils.cropImg import cropImg
from reid_utils.tracker_ import Tracker

class Reid_System:

    def __init__( self, camA_movie, camB_movie, jsonA_data, jsonB_data, camA_masked, camB_masked, filename, evaluate_gen=None):
        self.camA_video = camA_movie
        self.camB_video = camB_movie
        self.camA_json  = jsonA_data
        self.camB_json  = jsonB_data
        self.camC_json  = copy.deepcopy( self.camB_json )
        self.camA_mask  = camA_masked
        self.camB_mask  = camB_masked
        self.base_color = (5, 250, 247)
        self.drawing_   = draw_output.TrackingDraw( self.base_color )
        self.filename   = filename
        self.evaluate_  = evaluate_gen
        # 90 is time range, 1 ,2 ,3 is camID
        self.trackerA = Tracker(self.camA_mask, 90, 1)
        self.trackerB = Tracker(self.camB_mask, 90, 2)
        self.trackerC = Tracker(self.camB_mask, 90, 3)

        self.start_camera_index = "A"

        self.run()

    def updateTracker( self, json_data, video_frame, tracker ):
        copied_frame = copy.copy( video_frame )
        for i in range(len(json_data['timelines'])):
            curr_data = json_data['timelines'][i]
            for frame in curr_data['frames']:
                if frame['frame'] == self.frame_number:
                    crop_img = cropImg( frame['bbox'], frame['id'], frame['bytetrack_IOU'], copied_frame )
                    cv2.putText( video_frame, str(frame['id']), crop_img.textPosition , cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0,255,0), 2)
                    cv2.rectangle( video_frame , crop_img.x1y1, crop_img.x2y2, (0,255,0), 1)
                    track_id = int(frame['id'].split("_")[-1])
                    tracker.update( track_id, self.frame_number, crop_img, self.evaluate_ )

    def compareWithStartCameraTracker( self, json_data, video_frame, tracker, camA_tracker ):
        copied_frame   = copy.copy( video_frame )
        for i in range(len(json_data['timelines'])):
            curr_data = json_data['timelines'][i]
            for frame in curr_data['frames']:
                if frame['frame'] == self.frame_number:
                    crop_img = cropImg( frame['bbox'], frame['id'], frame['detect_confidence'], copied_frame )

                    cv2.putText( video_frame, str(frame['id']), crop_img.textPosition , cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0,255,0), 2)
                    cv2.rectangle( video_frame , crop_img.x1y1, crop_img.x2y2, (0,255,0), 1)
                    track_id = int(frame['id'].split('_')[-1])
                    if self.start_camera_index in frame['id'] and frame['id'] not in self.curr_list:
                        self.curr_list.append( frame['id'] )
                        continue
                    else:
                        reid_ID, score = tracker.compareWithStartCam( camA_tracker, crop_img, self.frame_number, self.curr_list, self.evaluate_, self.start_camera_index )
                        if reid_ID == -1:
                            pass
                        else:
                            if self.start_camera_index not in frame['id']:
                                original_id = frame['id']
                                curr_data['id'] = reid_ID
                                if curr_data['id'] not in self.curr_list:
                                    self.curr_list.append( curr_data['id'] )
                                for frame in curr_data['frames']:
                                    frame['id'] = reid_ID
                                    frame['pre_id'] = original_id
                                    frame['reid_confidence'] = score


    def run( self ):

        for frame_number in tqdm.tqdm(range( self.camA_video.maxMovieFrame )):
            self.curr_list  = []
            self.frame_number = frame_number
            self.camA_video.readFrame()
            self.camB_video.readFrame()
            self.new_image = copy.deepcopy( self.camB_video.currentFrame )

            self.updateTracker( self.camA_json, self.camA_video.currentFrame, self.trackerA )
            self.compareWithStartCameraTracker ( self.camB_json, self.camB_video.currentFrame, self.trackerB, self.trackerA )
            self.compareWithStartCameraTracker ( self.camC_json, self.new_image, self.trackerC, self.trackerA )
            #self.compareWithStartCameraTracker ( self.camB_json, self.camB_video.currentFrame, self.trackerB, self.trackerA )

            self.drawing_.FourCam_display( self.camA_video.currentFrame, self.camB_video.currentFrame, self.new_image , self.camB_video.currentFrame )
            self.drawing_.writeToVideo()
            if self.drawing_._endVideo is True:
                self.matchJSON()
                self.drawing_.releaseVideo()
                cv2.destroyAllWindows()
                break

        self.matchJSONB()
        self.drawing_.releaseVideo()

    def matchJSON(self):
        with open(f"output/reid_results/{self.filename}_reid.json", 'w') as jsonFile:
            json.dump(self.camB_json, jsonFile, indent=4, ensure_ascii=False)

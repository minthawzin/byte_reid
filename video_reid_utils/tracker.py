import numpy as np
from tracking_utils.Re_ID.reid_ import CosineMetric_ReID, QConv_Reid
from typing import List

class Track_ID:
    def __init__(self, track_id: int,removal_threshold: int, frame_id: int, bicycle_flag:int):
        self.track_id = track_id
        self.bicycle = bicycle_flag
        self.first_deep_features = []
        self.first_feature_features = []
        self.last_deep_features = []
        self.last_feature_features = []
        self.last_frame = frame_id
        self.removal_threshold = removal_threshold
        self.start_frame = frame_id
        self.feature_storage_ = 50

    def update(self, new_feature: np.ndarray, new_deep_feature, frame_id: int):
        if len(self.first_deep_features) >= self.feature_storage_:
            if len(self.last_deep_features) >= self.feature_storage_:
                self.last_deep_features.pop(0)
                self.last_feature_features.pop(0)
            self.last_deep_features.append(new_deep_feature)
            self.last_feature_features.append(new_feature)
        else:
            self.first_feature_features.append(new_feature)
            self.first_deep_features.append(new_deep_feature)
            self.last_deep_features.append(new_deep_feature)
            self.last_feature_features.append(new_feature)
        self.last_frame = frame_id


    def _get_first_features(self, track_id):
        if track_id == self.track_id:
            return self.first_deep_features, self.first_feature_features
        else:
            return []


    def _get_last_features(self, track_id):
        if track_id == self.track_id:
            return self.last_deep_features, self.last_feature_features
        else:
            return []

class Tracker:
    def __init__(self):
        self.tracked_ids = []
        self.tracks      = []
        self.reid_ids    = []
        
        self.feature_extractor = CosineMetric_ReID("tracking_utils/Re_ID/cosine_metric/weights/deepsort.t7")
        self.deep_extractor    = QConv_Reid("tracking_utils/Re_ID/qconv_reid/weights/pretrained_checkpoint.pth.tar")
        self.removal_threshold = 10
        self.feature_storage_  = 50
        self.compare_target_   = 5
        self.f_threshold       = 0.025
        self.d_threshold       = 0.5

    def update(self, track_id, frame_id, cv2_data, bicycle_flag):
        deep_features = self.deep_extractor.getFeatures([cv2_data])
        feature_features = self.feature_extractor.getFeatures([cv2_data])

        for track in self.tracks:
            if track.last_frame + self.removal_threshold < frame_id:
                track.deep_features = []
                track.feature_features = []
                self.tracks.remove(track)
                self.tracked_ids.remove(track.track_id)

        if track_id not in self.tracked_ids:
            new_track = Track_ID(track_id, self.removal_threshold, frame_id, bicycle_flag)
            self.tracked_ids.append(track_id)
            self.tracks.append(new_track)
            new_track.update(feature_features, deep_features, frame_id)
            return new_track.track_id
        else:
            for track in self.tracks:
                if track.track_id == track_id:
                    track.update(feature_features, deep_features, frame_id)
                    return track.track_id

        # remove memory

    def getLastFrame(self, id):
        for track in self.tracks:
            if track.track_id == id:
                return track.last_frame

        return None

    def compare(self, id, track_system, bicycle=None):
        for track in self.tracks:
            if track.track_id == id:
                id_track = track

        near_deep_feature, near_feature_feature = id_track._get_first_features(id)
        far_deep_feature, far_feature_feature = id_track._get_last_features(id)

        if len(near_deep_feature) < self.feature_storage_:
            return 0, 0, 0

        # get the 1 sec frame of each features
        near_deep_feature = near_deep_feature
        near_normal_feature = near_feature_feature
        # get the 5th frame of far features
        far_deep_feature = far_deep_feature
        far_normal_feature = far_feature_feature

        for index,feature in enumerate(near_deep_feature):
            if index == 0:
                continue
            if index == self.feature_storage_:
                continue
            # reduce processing time
            if index % self.compare_target_ == 0:
                output_result, reid_confidence = self.compareLastFeature(near_deep_feature[index], near_normal_feature[index], track_system, id, bicycle, id_track.last_frame)
                if output_result != 0:
                    return output_result, id, reid_confidence

        if output_result == 0:
            for index,feature in enumerate(far_deep_feature):
                if index == 0:
                    continue
                if index == self.feature_storage_:
                    continue
                # reduce processing time
                if index % self.compare_target_ == 0:
                    output_result, reid_confidence = self.compareFirstFeature(far_deep_feature[index], far_normal_feature[index], track_system, id, bicycle, id_track.last_frame)
                    if output_result != 0:
                        return output_result, id, reid_confidence

        return output_result, id, reid_confidence


    def compareLastFeature(self, deep_feature, normal_feature, track_system, id, bicycle=None, last_frame_id = None):
        for track in track_system.tracks:
            start_frame_threshold = track.start_frame - self.removal_threshold
            end_frame_threshold = track.last_frame + self.removal_threshold
            if last_frame_id >= start_frame_threshold and last_frame_id <= end_frame_threshold:
                feature_score = self.reIdentifyByFeature(normal_feature, self.feature_storage_, track.last_feature_features)
                similarity_score = self.reIdentifyByDeep(deep_feature, self.feature_storage_, track.last_deep_features)
                if feature_score < self.f_threshold and similarity_score < self.d_threshold:
                    if track.track_id in self.reid_ids and id > 20000:
                        return 0, 0
                    for curr_track in self.tracks:
                        if curr_track.track_id == id:
                            self.reid_ids.append(track.track_id)
                            return track.track_id, round(similarity_score,2)

            else:
                continue


        return 0, 0

    def compareFirstFeature(self, deep_feature, normal_feature, track_system, id, bicycle=None, last_frame_id=None):
        for track in track_system.tracks:
            start_frame_threshold = track.start_frame - self.removal_threshold
            end_frame_threshold = track.last_frame + self.removal_threshold
            if last_frame_id >= start_frame_threshold and last_frame_id <= end_frame_threshold:
                    feature_score = self.reIdentifyByFeature(normal_feature, self.feature_storage_, track.first_feature_features)
                    similarity_score = self.reIdentifyByDeep(deep_feature, self.feature_storage_, track.first_deep_features)
                    if feature_score < self.f_threshold and similarity_score < self.d_threshold:
                        if track.track_id in self.reid_ids and id > 20000:
                            return 0,0
                        for curr_track in self.tracks:
                            if curr_track.track_id == id:
                                self.reid_ids.append(track.track_id)
                                return track.track_id, round(similarity_score, 2)
            else:
                continue

        return 0, 0


    def checkSimilar(self, id, cv2_data, track_system, bicycle, frame_id):
        curr_deep_feature = self.deep_extractor.getFeatures([cv2_data])
        curr_feature = self.feature_extractor.getFeatures([cv2_data])
        output_result, reid_confidence = self.compareLastFeature(curr_deep_feature, curr_feature, track_system, id, bicycle, frame_id)
        if output_result == 0:
            output_result, reid_confidence = self.compareFirstFeature(curr_deep_feature, curr_feature, track_system, id, bicycle, frame_id)

        return output_result, id, reid_confidence


    def reIdentifyByDeep(self, curr_feature: np.ndarray, timeline_length: int, lost_features: List):
        similarity_score = 0
        frame_count = 0
        min_similarity_score = 1
        #self.reid_offset = 0.15
        for index,feature in enumerate(lost_features[:]):
            if index == 0:
                continue
            if index == timeline_length:
                continue
            if (index % self.compare_target_) == 0:
                #similar_ = self._cosine_distance(curr_feature, feature)
                #similar_ = similar_[0][0]
                similar_ = self.deep_extractor.similarityScore(curr_feature, feature, normalised_score=True)
                if similar_ < min_similarity_score:
                    min_similarity_score = similar_
                similarity_score += similar_

                frame_count += 1

        if frame_count == 0:
            # not similar (higher value)
            avg_similarity = 1

        else:
            avg_similarity = similarity_score/frame_count

        return avg_similarity

    def reIdentifyByFeature(self, curr_feature: np.ndarray, timeline_length: int, lost_features: List):
        similarity_score = 0
        frame_count = 0

        for index,feature in enumerate(lost_features[:]):
            if index == 0:
                continue
            if index == timeline_length:
                continue
            if (index % self.compare_target_) == 0:
                similar_ = self.feature_extractor.similarityScore(curr_feature, feature, normalised_score=True)
                similarity_score += similar_

                frame_count += 1

        if frame_count == 0:
            # not similar (higher value)
            avg_similarity = 1

        else:
            avg_similarity = similarity_score/frame_count

        return avg_similarity

import cv2, json, os
import numpy as np
import tqdm
from . import tracker
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import time
class Video_Reid:
    def __init__(self, video_A: str, video_B: str, json_A: str, json_B:str, A_person_get_off_bus=None, reid_area=None, B_person_get_off_bus=None):
        self.video_capA = cv2.VideoCapture(video_A)
        self.max_frames = int(self.video_capA.get(cv2.CAP_PROP_FRAME_COUNT))

        self.video_capB = cv2.VideoCapture(video_B)
        self.frame_id = 0
        with open(f'{json_A}') as f:
            self.json_data_A = json.load(f)
        with open(f'{json_B}') as f:
            self.json_data_B = json.load(f)
        self.trackerA = tracker.Tracker()
        self.trackerB = tracker.Tracker()
        fourcc = cv2.cv2.VideoWriter_fourcc('M','P','4','V')
        self.out = cv2.VideoWriter("combined_output.mp4", fourcc, 30.0, (1920,720))
        self.camA_person_bus = Polygon(A_person_get_off_bus)
        self.reid_area = Polygon(reid_area)
        self.camB_person_bus = Polygon(B_person_get_off_bus)
        self.matched_json_name = json_B.split('/')[-1].split(".")[0]
        self.compare_target_threshold = 10

        self.output_dir = "output/reid_results/"
        self.makeOutputDir()

    def makeOutputDir( self ):
        os.makedirs( self.output_dir, exist_ok=True )

    def run(self):
        start_time = time.time()

        print(f" Generating REID JSON : output/reid_results/{self.matched_json_name}_reid.json" )

        for frame_number in tqdm.tqdm(range(self.max_frames)):
            ret, self.vid_frame_A = self.video_capA.read()
            if ret is False:
                self.matchJSONB()
                return

            if ret is True:
                for i in range(len(self.json_data_A['timelines'])):
                    curr_data = self.json_data_A['timelines'][i]
                    for frame in curr_data['frames']:
                        if frame['frame'] == self.frame_id:
                            start_bbox = curr_data['frames'][0]['bbox']

                            bbox = frame['bbox']
                            bbox = [int(i) for i in bbox]
                            bicycle = curr_data['bicycle']
                            img_crop = self.vid_frame_A[int(bbox[1]):int(bbox[3]), int(bbox[0]):int(bbox[2])]
                            self.trackerA.update(frame['id'], self.frame_id, img_crop, bicycle)
                            bicycle = curr_data['bicycle']
                            if bicycle == 1:
                                cv2.putText(self.vid_frame_A, "bicycle", (bbox[0], bbox[1] - 40), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0), 2)

                            cv2.putText(self.vid_frame_A, str(frame['id']), (bbox[0], bbox[1] - 10), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0,255,0), 2)
                            cv2.rectangle(self.vid_frame_A, (bbox[0],bbox[1]), (bbox[2], bbox[3]), (0,255,0), 1)

            ret, self.vid_frame_B = self.video_capB.read()
            if ret is True:
                for i in range(len(self.json_data_B['timelines'])):
                    curr_data = self.json_data_B['timelines'][i]
                    is_present_in_frame = False
                    for frame in curr_data['frames']:
                        if frame['frame'] == self.frame_id:
                            is_present_in_frame = True
                            start_bbox = curr_data['frames'][0]['bbox']
                            bbox = frame['bbox']
                            bbox = [int(i) for i in bbox]
                            img_crop = self.vid_frame_B[int(bbox[1]):int(bbox[3]), int(bbox[0]):int(bbox[2])]
                            bicycle = curr_data['bicycle']
                            id = self.trackerB.update(frame['id'], self.frame_id, img_crop, bicycle)
                            if frame['id'] > 20000:
                                reid_id, original_id, reid_confidence = self.trackerB.checkSimilar(id, img_crop, self.trackerA, bicycle, self.frame_id)
                            else:
                                reid_id = frame['id']
                            if reid_id != 0:
                                if frame['id'] > 20000:
                                    curr_data['id'] = reid_id
                                    for frame in curr_data['frames']:
                                        frame['id'] = reid_id
                                        frame['pre_id'] = original_id
                                        frame['reid_confidence'] = reid_confidence
                                #break
                            id_ = frame['id']
                            bicycle = curr_data['bicycle']
                            #if bicycle == 1:
                            #    cv2.putText(self.vid_frame_B, "bicycle", (bbox[0], bbox[1] - 40), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0), 2)
                            cv2.putText(self.vid_frame_B, str(id_), (bbox[0], bbox[1] - 10), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0,255,0), 2)
                            cv2.rectangle(self.vid_frame_B, (bbox[0],bbox[1]), (bbox[2], bbox[3]), (0,255,0), 1)

                    if is_present_in_frame == False and curr_data['id'] > 20000:
                        id_of_b = curr_data['id']
                        last_frame_of_id = self.trackerB.getLastFrame(id_of_b)
                        id_bicycle = curr_data['bicycle']
                        if last_frame_of_id is None:
                            continue
                        if last_frame_of_id + self.compare_target_threshold > self.frame_id:
                            reid_id, original_id, reid_confidence = self.trackerB.compare(id_of_b, self.trackerA, bicycle)
                            if reid_id != 0:
                                curr_data['id'] = reid_id
                                for frame in curr_data['frames']:
                                    frame['id'] = reid_id
                                    frame['pre_id'] = original_id
                                    frame['reid_confidence'] = reid_confidence


            resize_frame_A = cv2.resize(self.vid_frame_A, (960, 720))
            resize_frame_B = cv2.resize(self.vid_frame_B, (960, 720))
            resize_frame = np.concatenate((resize_frame_A, resize_frame_B), axis=1)
            #self.out.write(resize_frame)
            cv2.imshow('output', resize_frame)
            key = cv2.waitKey(1) & 0xFF
            if key == ord('q'):
                cv2.destroyAllWindows()
                self.matchJSONB()
                return

            self.frame_id += 1
            avg_fps = (time.time() - start_time)/self.frame_id

        self.matchJSONB()
        return

    def matchJSONB(self):
        with open(f"output/reid_results/{self.matched_json_name}_reid.json", 'w') as jsonFile:
            json.dump(self.json_data_B, jsonFile, indent=4, ensure_ascii=False)

import glob, cv2, os, sys, numpy
sys.path.append('./')
from tracking_utils.Re_ID.reid_ import CosineMetric_ReID, QConv_Reid, Trans_ReID
from reid_accuracy.image_utils import file_information


def compareAccuracy( start_cam_img_dir, end_cam_img_dir, reid_model ):

    reIDs_list = []
    for images in glob.glob( start_cam_img_dir + '*' ):
        filename  = images.split('/')[-1]
        img_file  = cv2.imread( images )
        file_info = file_information.FileInfo( filename, img_file )

        for end_images in glob.glob( end_cam_img_dir + '*' ):
            end_filename = end_images.split('/')[-1]
            end_img_file = cv2.imread( end_images )
            endFile_info = file_information.FileInfo( end_filename , end_img_file )

            if file_info.trackId == endFile_info.trackId:
                if endFile_info.trackId not in reIDs_list:
                    train_feature = reid_model.getFeatures([img_file])
                    test_feature  = reid_model.getFeatures([end_img_file])
                    similarityScore = reid_model.similarityScore( train_feature, test_feature )
                    print( similarityScore )

                    display_img  = numpy.concatenate((img_file, end_img_file), axis=1)
                    cv2.imshow( "comparison", display_img )
                    cv2.waitKey(0)

def main():

    start_cam_img_dir = "reid_accuracy/evaluation_/bounding_box_train/"
    end_cam_img_dir   = "reid_accuracy/evaluation_/bounding_box_test/"

    transReid_weights = "tracking_utils/Re_ID/trans_reid/weights/vit_transreid_market.pth"
    transReid_model   = Trans_ReID(transReid_weights)

    compareAccuracy( start_cam_img_dir, end_cam_img_dir, transReid_model )

    #outputEvalutionResults( total_startIDs, total_REIDs )

if __name__ == '__main__':
    main()
    os._exit(0)

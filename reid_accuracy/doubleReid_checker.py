"""
    Re_ID checker
    @author: mtzin
    GlobalWalkers Co Inc.
"""

"""
Intended to run from the root of the repository with the command
python3 reid_accuracy/doubleReid_checker.py
"""

import sys, os, glob, cv2, shutil
sys.path.append('./')

from tracking_utils.Re_ID import reid_
from image_utils.image_info import ImageInformation

def getMostSimilarImage( similarity_score, image_info, compare_info ):
    output_image = None
    if similarity_score is None:
        similarity_score = image_info.QconvSimilarity
        output_image = image_info.outputImage( compare_info )

    else:
        if similarity_score < image_info.QconvSimilarity:
            pass
        else:
            similarity_score = image_info.QconvSimilarity
            output_image = image_info.outputImage( compare_info )

    return output_image, similarity_score

def main():
    cosine_weights_dir  = "tracking_utils/Re_ID/cosine_metric/weights/deepsort.t7"
    cosine_reid_model   = reid_.CosineMetric_ReID( cosine_weights_dir )

    qconv_weights_dir   = "tracking_utils/Re_ID/qconv_reid/weights/checkpoint_augment.pth.tar"
    qconv_reid_model    = reid_.QConv_Reid( qconv_weights_dir )

    target_dir          = "reid_accuracy/generated_images/target_images/0/"
    compare_dir         = "reid_accuracy/generated_images/compare_images/"

    reid_results_dir    = "reid_accuracy/generated_images/reid_results"

    try:
        shutil.rmtree( reid_results_dir )
        os.makedirs( reid_results_dir, exist_ok=True )
    except FileNotFoundError:
        os.makedirs( reid_results_dir, exist_ok=True )

    print(" Re-ID evaluating ... ")
    for index, image_file in enumerate(glob.glob( target_dir + "/*" )):
        image_info = ImageInformation( image_file )
        image_info.getCosineFeatures( cosine_reid_model )
        image_info.getQconvFeatures( qconv_reid_model )

        for compare_index, compare_image_folder in enumerate(glob.glob( compare_dir + "/*" )):
            similarity_score = None
            compare_detection = 0
            for compare_image_file in glob.glob( compare_image_folder + '/*'):
                compare_info = ImageInformation( compare_image_file )
                compare_info.getCosineFeatures( cosine_reid_model )
                compare_info.getQconvFeatures( qconv_reid_model )

                # compare features
                image_info.getCosineSimilarity( compare_info, cosine_reid_model )
                image_info.getQconvSimilarity( compare_info, qconv_reid_model )

                #output_image, similarity_score = getMostSimilarImage( similarity_score, image_info, compare_info )
                output_image = image_info.outputImage( compare_info )
                #if output_image is not None:
                #    cv2.imwrite( f"reid_accuracy/generated_images/reid_results/{index}_{compare_image_folder.split('/')[-1]}.png", output_image)
                cv2.imwrite( f"reid_accuracy/generated_images/reid_results/{index}_{compare_detection}_{compare_image_folder.split('/')[-1]}.png", output_image)
                compare_detection += 1

if __name__ == '__main__':
    main()
    os._exit(0)

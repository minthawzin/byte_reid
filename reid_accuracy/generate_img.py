"""
    Reid Accuracy Checker
    @author: mtzin
    GlobalWalkers Co.Inc
"""

from movie_utils import movie_utility, json_utility
from image_utils import image_generator
import os, cv2, copy, logging

"""
Intended to run from the root of the repository with the command
python3 reid_accuracy/generate_img.py
"""

def runSystem( target_movie_dir, target_json_dir, compare_movie_dir, compare_json_dir, cut_image_dir, cut_interval=1):
    target_movie  = movie_utility.MovieUtility( movie_dir = target_movie_dir, \
                                                cut_interval = cut_interval )

    target_json   = json_utility.JsonUtility( json_dir = target_json_dir )

    compare_movie = movie_utility.MovieUtility( movie_dir = compare_movie_dir, \
                                                cut_interval = cut_interval )
    compare_json  = json_utility.JsonUtility( json_dir = compare_json_dir )

    if target_movie.maxMovieFrame == 0 or compare_movie.maxMovieFrame == 0:
        print("Movie Path/Data is Invalid. Please try again")
        os._exit(0)

    img_generate_ = image_generator.ImageGenerator( cut_image_dir )
    img_generate_.generateTargetImages( target_movie, target_json )
    img_generate_.generateCompareImages( compare_movie, compare_json )

def main() -> None:

    ###################
    # 1 Camera Matching ( Same compare and target directories )
    ###################
    target_movie_dir  = "./reid_accuracy/camB/CameraB_20211118_1230-1240_S.MP4"
    target_json_dir   = "./output/tracking_results/CameraB_20211118_1230-1240_S.json"
    compare_movie_dir = "./reid_accuracy/camB/CameraB_20211118_1230-1240_S.MP4"
    compare_json_dir  = "./output/tracking_results/CameraB_20211118_1230-1240_S.json"

    ###################
    # 2 Camera Matching ( Different compare and target directories )
    ###################
    #target_movie_dir  = "./reid_accuracy/camA/CameraA_20211118_1230-1240_S.MP4"
    #target_json_dir   = "./output/tracking_results/CameraA_20211118_1230-1240_C.json"
    #compare_movie_dir = "./reid_accuracy/camB/CameraB_20211118_1230-1240_S.MP4"
    #compare_json_dir  = "./output/tracking_results/CameraB_20211118_1230-1240_S.json"

    cut_image_dir = "./reid_accuracy/generated_images/"
    cut_interval  = 1 # seconds

    runSystem( target_movie_dir, target_json_dir, compare_movie_dir, compare_json_dir, cut_image_dir, cut_interval )

if __name__ == '__main__':
    main()
    os._exit(0)

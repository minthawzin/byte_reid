from movie_utils import movie_utility, json_utility
import os, shutil

def runCutImages( movie_dir, json_dir, out_dir, cut_interval ):

    movie_ = movie_utility.MovieUtility( movie_dir, cut_interval )
    json_  = json_utility.JsonUtility( json_dir )

    if movie_.maxMovieFrame == 0:
        print("Movie Path/Data is Invalid. Please try again")
        os._exit(0)

    for frame in range( movie_.maxMovieFrame ):
        movie_.readFrame()
        if frame % movie_.cutFrameInterval == 0:
            json_.getCurrentDetections( frame )
            movie_.overlayTrackingResults( json_.currentDetections, out_dir )


def main():

    movie_dir = "reid_accuracy/camA/CameraA_20211118_1230-1240_C.MP4"
    base_filename = movie_dir.split('/')[-1].split('.')[0]
    json_dir  = "output/tracking_results/CameraA_20211118_1230-1240_C.json"
    out_dir   = f"reid_accuracy/bytetrack_cut/{base_filename}"

    try:
        shutil.rmtree( out_dir )
        os.makedirs( out_dir, exist_ok=True )
    except FileNotFoundError:
        os.makedirs( out_dir, exist_ok=True )

    cut_interval = 1 # second

    runCutImages( movie_dir, json_dir, out_dir, cut_interval )

if __name__ == '__main__':
    main()
    os._exit(0)

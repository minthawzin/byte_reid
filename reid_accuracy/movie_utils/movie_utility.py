"""
    movie_utility.py
    @author: mtzin
    Copyright GlobalWalkers Inc
"""

import cv2, copy
import numpy as np

class MovieUtility:

    @property
    def movieDir( self ) -> str:
        return self.__movie_dir

    @property
    def movieData( self ) -> cv2.VideoCapture:
        return self.__movie_data

    @property
    def FPS( self ) -> int:
        fps = int(self.movieData.get(cv2.CAP_PROP_FPS))
        return fps

    @property
    def maxMovieFrame( self ) -> int:
        max_frames = int(self.movieData.get(cv2.CAP_PROP_FRAME_COUNT))
        return max_frames

    @property
    def cutFrameInterval( self ) -> int:
        # cut_interval * FPS
        return int(self.cut_interval_seconds * self.FPS)

    @property
    def currentFrameId( self ) -> int:
        return self.__frame_id

    @property
    def currentFrame( self ) -> np.ndarray:
        return self.__frame

    @property
    def cameraID( self ) -> str:
        # assume movie directory is in the format
        # /dir/CameraA_20211118_0730-0740.MP4
        return str(self.movieDir.split('/')[-1].split('_')[0])

    def __init__( self, movie_dir: str, cut_interval: int = 1 ) -> None:
        # cut iterval = 1 second
        self.__movie_dir = movie_dir
        self.__frame_id  = -1
        self.__frame     = None
        self.__movie_data = cv2.VideoCapture( self.movieDir )
        self.cut_interval_seconds = cut_interval

    def readFrame( self ):
        res, self.__frame = self.movieData.read()
        if res is False:
            self.__frame = None
        else:
            self.__frame_id += 1

    def overlayTrackingResults( self, tracking_results, out_dir ):
        overlay_img = copy.deepcopy( self.currentFrame )
        for tracking in tracking_results:
            #print( tracking.x1y1 , tracking.x2y2, tracking.track_id )
            cv2.rectangle( overlay_img, tracking.x1y1, tracking.x2y2, (0,255,0), 2 )
            cv2.putText( overlay_img, str(tracking.track_id), tracking.textPosition,  cv2.FONT_HERSHEY_SIMPLEX, \
                1, (0, 255, 0), 2 )
            cv2.imwrite( f"{out_dir}/{self.currentFrameId}.png", overlay_img )

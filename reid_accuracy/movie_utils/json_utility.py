import json

class JsonUtility:

    @property
    def jsonDir( self ) -> str:
        return self.__json_dir

    @property
    def jsonData( self ) -> dict:
        return self.__json_data

    @property
    def currentDetections( self ) -> list:
        return self.__detections

    def __init__( self, json_dir:str ) -> None:
        self.__json_dir = json_dir
        self.loadJSON()

    def loadJSON( self ):
        with open(self.jsonDir) as json_file:
            self.__json_data = json.load(json_file)

    def getCurrentDetections( self, frame_id : int ):
        self.__detections = []
        for i in range(len(self.jsonData['timelines'])):
            curr_data = self.jsonData['timelines'][i]
            for frame in curr_data['frames']:
                if frame['frame'] == frame_id:
                    bounding_box = TrackingBox( frame['bbox'], frame['id'] )
                    self.__detections.append(bounding_box)

class TrackingBox:
    @property
    def bbox( self ):
        return (self.x1, self.y1, self.x2, self.y2)

    @property
    def x1y1( self ):
        return (self.x1, self.y1)

    @property
    def x2y2( self ):
        return (self.x2, self.y2)

    @property
    def textPosition( self ):
        return (self.x1, self.y1 - 20)

    def __init__( self, x1y1x2y2: list, id: int):
        self.x1, self.y1, self.x2, self.y2 = [int(i) for i in x1y1x2y2]
        self.width = self.x2 - self.x1
        self.height = self.y2 - self.y1
        self.track_id  = id

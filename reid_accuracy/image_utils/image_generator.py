import cv2, os, copy, shutil, tqdm

class ImageGenerator:

    def __init__( self, generate_dir ):
        self.generate_dir = generate_dir
        self.cut_scene_dir     = f"{self.generate_dir}/cut_images/"
        self.target_image_dir  = f"{self.generate_dir}/target_images/"
        self.compare_image_dir = f"{self.generate_dir}/compare_images/"

        self.cleanFolders()

        os.makedirs( self.cut_scene_dir, exist_ok=True )
        os.makedirs( self.target_image_dir, exist_ok=True )
        os.makedirs( self.compare_image_dir, exist_ok=True )

    def cleanFolders( self ):
        try:
            shutil.rmtree(self.cut_scene_dir)
            shutil.rmtree(self.target_image_dir)
            shutil.rmtree(self.compare_image_dir)
        except FileNotFoundError:
            pass

    def generateTargetImages( self, target_movie, target_json ):
        # only generate first frame of the video as the target detections
        for frame_number in range( target_movie.maxMovieFrame ):
            target_movie.readFrame()
            if target_movie.currentFrame is None:
                return

            if frame_number % target_movie.cutFrameInterval == 0:
                self.generateCutImages( target_movie )
                # first frame is target frame, the rest is compare frame
                if frame_number == 0:
                    folder_output = f"{self.target_image_dir}/{target_movie.currentFrameId}"
                    os.makedirs (folder_output, exist_ok=True )
                    target_json.getCurrentDetections( target_movie.currentFrameId )
                    self.generateCroppedImages( target_movie.currentFrame, target_json.currentDetections, folder_output )

    def generateCompareImages( self, compare_movie, compare_json ):
        for frame_number in tqdm.tqdm(range( compare_movie.maxMovieFrame )):
            compare_movie.readFrame()
            if compare_movie.currentFrame is None:
                return

            if frame_number % compare_movie.cutFrameInterval == 0:
                self.generateCutImages( compare_movie )
                # first frame is target frame, the rest is compare frame
                folder_output = f"{self.compare_image_dir}/{compare_movie.currentFrameId}"
                os.makedirs (folder_output, exist_ok=True )
                compare_json.getCurrentDetections( compare_movie.currentFrameId )
                self.generateCroppedImages( compare_movie.currentFrame, compare_json.currentDetections, folder_output )

    def generateCutImages( self, movie ):
        image_save_filename = f"{self.cut_scene_dir}/{movie.cameraID}_{movie.currentFrameId}.png"
        cv2.imwrite( image_save_filename, movie.currentFrame )

    def generateCroppedImages( self, frame, current_detections, folder_output ):
        image_id = 0
        for detection in current_detections:
            crop_save_filename = f"{folder_output}/detections_{image_id}.png"
            copy_frame = copy.copy( frame )
            crop_img   = copy_frame[detection.y1:detection.y2, detection.x1:detection.x2]
            cv2.imwrite( crop_save_filename, crop_img )
            image_id += 1


class REID_Image_Generator(ImageGenerator):

    def __init__( self, generate_dir, evaluate_mode ):
        super().__init__( generate_dir )
        self.evaluate_mode  = evaluate_mode
        self.test_img_dir   = f"{self.generate_dir}/bounding_box_test/"
        self.train_img_dir  = f"{self.generate_dir}/bounding_box_train/"

        # to avoid multiple IDS
        self.save_train_ids = []
        self.save_test_ids  = []

        self.cleanFolders()
        os.makedirs( self.test_img_dir, exist_ok=True )
        os.makedirs( self.train_img_dir, exist_ok=True )

    def cleanFolders( self ):
        try:
            shutil.rmtree(self.cut_scene_dir)
            shutil.rmtree(self.target_image_dir)
            shutil.rmtree(self.compare_image_dir)
            shutil.rmtree(self.test_img_dir)
            shutil.rmtree(self.train_img_dir)
        except FileNotFoundError:
            pass

    def saveTrainImage( self, img, track_id, frame_id, camID ):
        file_format = f"{track_id:04}_c{camID}s{camID}_{frame_id:06}_01.jpg"
        file_path   = f"{self.train_img_dir}/{file_format}"
        self.save_train_ids.append( track_id )
        cv2.imwrite( file_path, img )

    def saveTestImage( self, img, track_id, frame_id, camID ):
        if track_id in self.save_test_ids:
            return

        id = int(track_id.split("_")[-1])
        file_format = f"{id:04}_c{camID}s{camID}_{frame_id:06}_01.jpg"
        file_path   = f"{self.test_img_dir}/{file_format}"
        self.save_test_ids.append( track_id )
        cv2.imwrite( file_path, img )

class FileInfo:

    @property
    def trackId( self ):
        return int(self.filename.split("_")[0])

    @property
    def frameNumber( self ):
        return int(self.filename.split("_")[2])

    def __init__( self, filename, cv_img ):
        self.filename = filename
        self.img      = cv_img

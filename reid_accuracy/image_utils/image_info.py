import cv2
import numpy as np

class ImageInformation:

    @property
    def imageDir( self ):
        return self.__image_dir

    @property
    def imageData( self ):
        return self.__image_data

    @property
    def cosineFeatures( self ):
        return self.__cosine_features

    @property
    def qconvFeatures( self ):
        return self.__qconv_features

    @property
    def CosineSimilarity( self ):
        value = round(self.__cosine_similarity, 3)
        return str(value)

    @property
    def QconvSimilarity( self ):
        value = round(self.__qconv_similarity, 3)
        return str(value)

    def __init__( self, image_dir ):
        self.__image_dir  = image_dir
        self.__image_data = None
        self.__cosine_features   = None
        self.__qconv_features    = None
        self.__cosine_similarity = None
        self.__qconv_similarity  = None

        self.getImage()

    def getImage( self ):
        if self.__image_dir is not None:
            self.__image_data = cv2.imread( self.imageDir )

    def getCosineFeatures( self, cosine_reid_model ):
        self.__cosine_features = cosine_reid_model.getFeatures( [self.imageData] )

    def getQconvFeatures( self, qconv_reid_model ):
        self.__qconv_features = qconv_reid_model.getFeatures( [self.imageData] )

    def getCosineSimilarity( self, compare_image_info , cosine_reid_model ):
        self.__cosine_similarity = cosine_reid_model.similarityScore( self.cosineFeatures, compare_image_info.cosineFeatures )

    def getQconvSimilarity( self, compare_image_info, qconv_reid_model ):
        self.__qconv_similarity = qconv_reid_model.similarityScore( self.qconvFeatures, compare_image_info.qconvFeatures )

    def outputImage( self, compare_image_info ):
        target_img  = cv2.resize(self.imageData, (240, 240))
        compare_img = cv2.resize(compare_image_info.imageData, (240, 240))
        whiteblankimage = 255 * np.ones(shape=[240, 240, 3], dtype=np.uint8)

        if self.__cosine_similarity is not None:
            cv2.putText(whiteblankimage, text=f'CosineSimilarity: {self.CosineSimilarity}', org=(20, 20),
                    fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.5, color=(0, 0, 0),
                    thickness=1, lineType=cv2.LINE_AA)
        if self.__qconv_similarity is not None:
            cv2.putText(whiteblankimage, text=f'QconvSimilarity: {self.QconvSimilarity}', org=(20, 60),
                    fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.5, color=(0, 0, 0),
                    thickness=1, lineType=cv2.LINE_AA)

        combined_image = np.concatenate(( target_img, compare_img ), axis=1)
        combined_image = np.concatenate(( combined_image, whiteblankimage), axis=1)

        return combined_image

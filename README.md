# HATCH TECHNOLOGY

## System setup
```
- conda env create -f conda_env_ubuntu/byte_reid_requirements.yaml
- conda activate byte_reid
```

### 3.4 Person Tracking Collection + Person Area Analysis

#### Set the files directory and path to run inside track.py

- movie_dir     : directory of the movie to run
- mask_area_dir : Use camA_mask.yaml for camerA and camB_mask.yaml for cameraB

    ```   
    movie_dir     = "reid_accuracy/camB/CameraB_20211118_0730-0740_C.MP4"

    # need to change accroding to camera , use camA_mask for cameraA and camB_mask for cameraB
    mask_area_dir = "mask_utils/camB_mask.yaml" 
    # camA => start from 1, camB => start from 20001
    starting_id   = 20001
    ```

#### Running system to get tracking JSON output

```
python3 track.py
```

#### Check the JSON output

The output JSON will be saved inside output/tracking_results/<video_filename>.json

#### Check tracking + area Analysis JSONs

- CamA
```
python3 movie_utils/check_track_result.py --vid reid_accuracy/camA/CameraA_20211118_0730-0740_C.MP4 --json output/tracking_results/CameraA_20211118_0730-0740_C.json
```
- CamB
```
python3 movie_utils/check_track_result.py --vid reid_accuracy/camB/CameraB_20211118_0730-0740_C.MP4 --json output/tracking_results/CameraB_20211118_0730-0740_C.json
```

### 5.2 ID Matching Re-ID
- Prequisites for this function
    - CameraA Video
    - CameraB Video
    - CameraA Tracking JSON (3.4 Person Tracking + Person Analysis)
    - CameraB Tracking JSON (3.4 Person Tracking + Person Analysis)
    
    
- If the prerequisites are passed, please edit the following parameters inside id_matcher.py
    ```
    camA_path  = "reid_accuracy/camA/CameraA_20211118_0730-0740_C.MP4"
    camB_path  = "reid_accuracy/camB/CameraB_20211118_0730-0740_C.MP4"
    jsonA_path = "output/tracking_results/CameraA_20211118_0730-0740_C.json"
    jsonB_path = "output/tracking_results/CameraB_20211118_0730-0740_C.json"
    ```

- Run the ID Matching system
    ```
    python3 id_matcher.py
    ```
    
- Check the REID results
The output JSON will be saved inside output/reid_results/<video_filename>_reid.json
```
python3 movie_utils/check_track_result.py --vid reid_accuracy/camB/CameraB_20211118_0730-0740_C.MP4 --json output/reid_results/CameraB_20211118_0730-0740_C_reid.json
```


## Running REID evaluation

- Make sure JSON file is already generated for the video
- If JSON files are not generated,
    - JSON_tracking_results.zip:  https://drive.google.com/drive/folders/1QvJu0fypXtwZshLnNwDU-GnJbExYS7cL?usp=sharing

### Generate Images from JSON and source video

- Please check the settings inside the generate_img.py

    ```
    ###################
    # 1 Camera Matching ( Same compare and target directories )
    ###################
    # target_movie_dir  = "./reid_accuracy/camA/CameraA_20211118_0730-0740_C.MP4"
    # target_json_dir   = "./output/tracking_results/CameraA_20211118_0730-0740_C.json"
    # compare_movie_dir = "./reid_accuracy/camA/CameraA_20211118_0730_0730_C.MP4"
    # compare_json_dir  = "./output/tracking_results/CameraA_20211118_0730-0740_C.json"


    ###################
    # 2 Camera Matching ( Different compare and target directories )
    ###################
    target_movie_dir  = "./reid_accuracy/camA/CameraA_20211118_0730-0740_C.MP4"
    target_json_dir   = "./output/tracking_results/CameraA_20211118_0730-0740_C.json"
    compare_movie_dir = "./reid_accuracy/camB/CameraB_20211118_0730-0740_C.MP4"
    compare_json_dir  = "./output/tracking_results/CameraB_20211118_0730-0740_C.json"

    cut_image_dir = "./reid_accuracy/generated_images/"
    cut_interval  = 1 # seconds
    ```

- Set the json and movie directories accordingly to your needs

- Finally run the following command to get the cut images and cropped images
```
python3 reid_accuracy/generate_img.py
```

### Check output of image generation
- Please check the directory reid_accuracy/generated_images/
    - Compare Cropped Images : reid_accuracy/generated_images/compare_images/<frame_id>/detections_i.png
    - Cut Images             : reid_accuracy/generated_images/cut_images/<CameraId>_<frame_id>.png
    - Target Images          : reid_accuracy/generated_images/0/detections_i.png

### Run Cosine Similarity comparison only
```
python3 reid_accuracy/singleReid_checker.py
```

### Run Cosine Similarity with QConv Similarity
```
python3 reid_accuracy/doubleReid_checker.py
```

### Check REID results

- The reid comparisons results can be checked in reid_accuracy/generated_images/reid_results/

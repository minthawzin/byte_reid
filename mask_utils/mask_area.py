from file_utils import yaml_parser
from shapely.geometry.polygon import Polygon

class MaskArea:

    @property
    def maskFile( self ):
        return self.__mask_file

    @property
    def maskData( self ):
        return self.__mask_data

    @property
    def polygonData( self ):
        return self.__polygon_mask

    @property
    def areaAnalysis( self ):
        self.__area_analysis = {}
        for key, value in self.polygonData.items():
            if key == 'person_area' or key == 'bus_area':
                continue
            self.__area_analysis.update({
                key: value
            })

        return self.__area_analysis


    def __init__( self, mask_file ):
        self.__mask_file = mask_file

        self.loadMaskData()

    def loadMaskData( self ):
        self.__mask_data = yaml_parser.YamlParser( config_file=self.maskFile )

        self.__polygon_mask = {}
        for key, value in self.maskData.items():
            if key == 'merge_from_file' or key == 'merge_from_dict':
                continue

            polygon_data = Polygon( value )
            polygon_dict = {
                key: polygon_data
            }
            self.__polygon_mask.update(polygon_dict)

    def loadSpecificArea( self, area_key ):
        polygon_area = area_key
        try:
            return self.__polygon_mask[ area_key ]
        except KeyError:
            return None

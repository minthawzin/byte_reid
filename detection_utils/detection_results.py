import torch, os, sys
from pathlib import Path

FILE = Path(__file__).resolve()
ROOT = FILE.parents[0]  # YOLOv5 root directory
if str(ROOT) not in sys.path:
    sys.path.append(str(ROOT))  # add ROOT to PATH
ROOT = Path(os.path.relpath(ROOT, Path.cwd()))  # relative
from utils.general import xyxy2xywh, scale_coords
from shapely.geometry import Point


class DetectionResult:

    @property
    def original_image_tensor( self ):
         original_image_tensor = torch.tensor(self.__original_img_shape)[[1, 0, 1, 0]].to('cuda:0')  # normalization gain whwh
         return original_image_tensor

    @property
    def originalImgShape( self ):
        return self.__original_img_shape

    @property
    def imgHeight( self ):
        return self.originalImgShape[0]

    @property
    def imgWidth( self ):
        return self.originalImgShape[1]

    @property
    def classNames( self ):
        return self.__class_names

    @property
    def getDetectionResults( self ):
        return self.__detection_list

    @property
    def numOfDetections( self ):
        return len(self.__detection_list)

    @property
    def maskData( self ):
        return self.__mask_data

    def __init__( self, model_loaded, mask_data=None ):
        self.__class_names = model_loaded.names
        self.__mask_data   = mask_data

    def inputDetection( self, detection_results, original_img_shape ):
        self.__detection_list = []
        self.__original_img_shape  = original_img_shape
        self.__detection_results = detection_results
        for detection in detection_results:
            for value in detection:
                x1y1x2y2    = self.getXYXY(value[:4])
                confidence  = round(value[4].item(), 2)
                class_index = int(value[5].item())
                class_name  = self.classNames[class_index]
                if self.maskData is not None:
                    valid_ = self.checkWithinMaskData( x1y1x2y2, class_name )
                    if valid_ == True:
                        detection_ = Detection( x1y1x2y2, confidence, class_name )
                        self.__detection_list.append(detection_)
                    else:
                        pass
                else:
                    detection_ = Detection( x1y1x2y2, confidence, class_name )
                    self.__detection_list.append(detection_)


    def checkWithinMaskData( self, bbox, class_name ):
        if class_name == 'person':
            polygon_area = self.maskData.loadSpecificArea( 'person_area' )
        elif class_name == 'bus':
            polygon_area = self.maskData.loadSpecificArea( 'bus_area' )
        else:
            polygon_area = self.maskData.loadSpecificArea( 'person_area' )

        x_c = (bbox[2] + bbox[0]) / 2
        y   =  bbox[3]
        area_analysis_point = Point( x_c, y )
        if polygon_area.contains( area_analysis_point ):
            return True
        else:
            return False

    def getXYXY( self, xyxy_normalised ):
        xyxy_clone = xyxy_normalised.detach().clone()
        tensor_xyxy = xyxy_clone.view( 1,4 )
        xywh = xyxy2xywh( tensor_xyxy / self.original_image_tensor )
        xywh = xywh.view(-1).tolist()
        x1y1x2y2 = self.denormalise( xywh )
        return x1y1x2y2

    def denormalise( self, xywh ):
        """
        Helper function to denormalise the coordinates
        Parameters:
            xyxy(List): x_c, y_c, width, height normalised coords
            original_img(np.ndarray): original_img data
        Returns:
            [x1,y1,x2,y2](List): denormalised coords
        """
        x_c, y_c, width, height = xywh
        x1 = int((x_c - width/2) * self.imgWidth)
        y1 = int((y_c - height/2) * self.imgHeight)
        x2 = int((x_c + width/2) * self.imgWidth)
        y2 = int((y_c + height/2) * self.imgHeight)
        return [x1, y1, x2, y2]

class Detection:

    @property
    def bbox( self ):
        return self.__bbox

    @property
    def confidence( self ):
        return self.__confidence

    @property
    def class_name( self ):
        return self.__class_name

    @property
    def x1y1( self ):
        return tuple(self.bbox[:2])

    @property
    def x2y2( self ):
        return tuple(self.bbox[2:4])

    def __init__( self, bbox, confidence, class_name ):
        self.__bbox = bbox
        self.__confidence = confidence
        self.__class_name = class_name

from pathlib import Path
import sys, os, glob, cv2, pandas, numpy, torch, json
from detection_utils import detection_results
import torch.backends.cudnn as cudnn
torch.cuda.empty_cache()
FILE = Path(__file__).resolve()
ROOT = FILE.parents[0]  # YOLOv5 root directory
if str(ROOT) not in sys.path:
    sys.path.append(str(ROOT))  # add ROOT to PATH
ROOT = Path(os.path.relpath(ROOT, Path.cwd()))  # relative

from models.common import DetectMultiBackend
from utils.torch_utils import select_device
from utils.augmentations import letterbox
from utils.general import non_max_suppression, scale_coords


class Detect_:

    @property
    def maskData( self ):
        return self.__mask_data

    def __init__( self, weights_dir, mask_data = None):
        self.img_size    = [1280, 1280]
        self.device      = select_device('0') # or  'cpu'
        self.classes     = [0, 1, 5]
        self.iou_thresh  = 0.5
        self.confidence  = 0.05
        self.weights_dir = weights_dir
        self.half        = True
        self.detect_conf = 0.3
        self.__mask_data = mask_data
        self.setupSystem()

    def setupSystem( self ):
        torch.cuda.empty_cache()
        self.model_loaded = DetectMultiBackend( self.weights_dir, device=self.device )
        self.model_loaded.model.half()
        self.model_loaded.warmup(imgsz=(1, 3, *self.img_size), half=self.half)  # warmup
        self.detection_results = detection_results.DetectionResult( self.model_loaded, self.maskData )

        cudnn.benchmark = True  # set True to speed up constant image size inference

    def setupOutputJSON( self, file_loader ):
        self.output_json  = []
        self.output_json_dir = f"output/detection_results/{file_loader.movieName}.json"
        self.object_count = 1
        self.frame_id = -1

    def preprocess2Yolov5( self, img_data ):
        '''
            YOLOV5 Preprocessing Based on YOLOv5 datasets.py
        '''
        img = letterbox(img_data, self.img_size, stride=self.model_loaded.stride, auto=self.model_loaded.pt)[0]
        # Convert
        img = img.transpose((2, 0, 1))[::-1]  # HWC to CHW, BGR to RGB
        resized_img = numpy.ascontiguousarray(img)
        img_tensor = torch.from_numpy(resized_img).to(self.device)
        img_tensor = img_tensor.half() if self.half else img_tensor.float()  # uint8 to fp16/32
        img_tensor /= 255  # 0 - 255 to 0.0 - 1.0

        if len(img_tensor.shape) == 3:
            img_tensor = img_tensor[None]  # expand for batch dim

        return resized_img, img_tensor

    def rescaleCoords( self ):
        detections = []
        for index, detection in enumerate( self.prediction ):
            if len(detection) != 0:
                # rescale detections to original image size
                detection[:, :4] = scale_coords(self.processed_tensor.shape[2:], detection[:, :4], self.original_img.shape).round()
                detections.append(detection)

        return detections

    def inference( self, img_data, frame_id ):
        self.frame_id = frame_id
        with torch.no_grad():
            self.original_img  = img_data
            self.resized_img, self.processed_tensor = self.preprocess2Yolov5( self.original_img )
            self.prediction    = self.model_loaded( self.processed_tensor )
            self.prediction    = non_max_suppression(self.prediction, self.confidence, self.iou_thresh, self.classes, max_det=100)
            processed_detections = self.rescaleCoords()
            self.detection_results.inputDetection(processed_detections, self.original_img.shape )
            self.addToJSON()

    def addToJSON( self ):
        for detection in self.detection_results.getDetectionResults:
            if detection.confidence > self.detect_conf:
                json_dict = {
                    "category"  : detection.class_name,
                    "frame"     : self.frame_id,
                    "object_id" : self.object_count,
                    "bbox"      : detection.bbox,
                    "confidence": detection.confidence,
                }
                self.output_json.append(json_dict)
                self.object_count += 1

    def outputJSON( self ):
        output_json = {
            "results": self.output_json
        }
        os.makedirs(os.path.dirname(self.output_json_dir), exist_ok=True)
        with open(f"{self.output_json_dir}", 'w') as jsonFile:
            json.dump( output_json, jsonFile, indent=4, ensure_ascii=False )

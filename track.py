import glob, os, cv2, time
from file_utils import *
from detection_utils import *
from drawing_utils import draw_output
from tracking_utils import *
from mask_utils import mask_area
from tqdm import tqdm

def runTracking( movieLoader, detector, tracker, drawer, camera_index ):
    start_time = time.time()
    for frame_number in tqdm(range( movieLoader.maxMovieFrame )):
        movieLoader.readFrame()
        detector.inference( movieLoader.currentFrame, movieLoader.frameId )
        tracker.updateFrame( detector.detection_results.getDetectionResults, movieLoader )
        drawer.draw( movieLoader.currentFrame, tracker.getTrackResults )
        drawer.writeToVideo()

        if drawer._endVideo is True:
            break

    drawer.releaseVideo()
    tracker.outputJSON( camera_index )
    detector.outputJSON()
    #print(f"Time : {int(total_time_taken)}")
    #print(f"FPS  : {int(total_time_taken)/movieLoader.maxMovieFrame}")

def main():

    weights_dir   = "detection_utils/weights/yolov5s6.pt"
    bytetrack_cfg = "tracking_utils/ByteTrack/track_config.yaml"
    camera_index  = "B"

    # person coming from left, choose "RIGHT" for right direction, None for all direction
    interested_direction = None

    movie_dir     = f"reid_accuracy/cam{camera_index}/Camera{camera_index}_20211118_0730-0740_C.MP4"
    #movie_dir     = "reid_accuracy/camB/CameraB_20211118_0730-0740_C.MP4"
    #movie_dir     = "cropped_videos/camA/day1/CameraA_20211118_0730-0740.MP4"
    #movie_dir      = f"cropped_videos/{camera_}/day1/CameraB_20211118_0730-0740.MP4"
    # need to change accroding to camera ROI region
    #mask_area_dir = "mask_utils/camA_mask.yaml"
    mask_area_dir = f"mask_utils/cam{camera_index}_mask.yaml"
    # camA => start from 1, camB => start from 20001
    starting_id   = 1

    base_color    = (5, 250, 247)
    loaded_file   = file_loader.FileLoader( movie_dir )
    masked_data   = mask_area.MaskArea( mask_area_dir )
    detection_    = detect_.Detect_( weights_dir, masked_data )
    detection_.setupOutputJSON( loaded_file )
    tracking_     = tracker_.Tracker( bytetrack_cfg, loaded_file.movieLoader, masked_data, starting_id, interested_direction )

    print(f" Generating tracking JSON : output/tracking_results/{tracking_.sourceVideoName}.json " )
    drawing_      = draw_output.TrackingDraw( base_color )
    runTracking( loaded_file.movieLoader, detection_, tracking_, drawing_, camera_index )

if __name__ == '__main__':
    main()
    os._exit(0)

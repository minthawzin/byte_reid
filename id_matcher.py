import json, glob, os, argparse, yaml
from video_reid_utils import video_reid
from file_utils import yaml_parser

def getArguments():
    args = argparse.ArgumentParser()
    args.add_argument("--camA", required=True, help="Path to the video file A")
    args.add_argument("--camB", required=True, help="Path to the video file B")
    args.add_argument('--jsonA', required=False, help="Path to the json file A")
    args.add_argument('--jsonB', required=False, help="Path to the json file B")
    #args.add_argument("--det", help="Path to the detection json file")
    arguments = args.parse_args()

    return arguments

def main():
    # cmd_args = getArguments()
    camA_path  = "reid_accuracy/camA/CameraA_20211118_0730-0740_C.MP4"
    camB_path  = "reid_accuracy/camB/CameraB_20211118_0730-0740_C.MP4"
    jsonA_path = "output/tracking_results/CameraA_20211118_0730-0740_C.json"
    jsonB_path = "output/tracking_results/CameraB_20211118_0730-0740_C.json"

    # for file in glob.glob(jsonA_path + '*'):
        #filename = file.split('/')[-1].split('.')[0]
        #date_time = '_'.join(map(str, filename.split('_')[1:]))
    video_reid_system = video_reid.Video_Reid(camA_path, camB_path, jsonA_path, jsonB_path)
    video_reid_system.run()

if __name__ == '__main__':
    main()
    os._exit(0)

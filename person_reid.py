import json, glob, os, argparse, yaml
from reid_utils import reid_system
from file_utils import yaml_parser, file_loader
from mask_utils import mask_area
from reid_accuracy.image_utils.image_generator import REID_Image_Generator

def main():
    camA_path  = "reid_accuracy/camA/CameraA_20211118_0730-0740_C.MP4"
    camB_path  = "reid_accuracy/camB/CameraB_20211118_0730-0740_C.MP4"
    jsonA_path = "output/tracking_results/CameraA_20211118_0730-0740_C.json"
    jsonB_path = "output/tracking_results/CameraB_20211118_0730-0740_C.json"
    #camA_path  = "cropped_videos/camA/day1/CameraA_20211118_0730-0740.MP4"
    #camB_path  = "cropped_videos/camB/day1/CameraB_20211118_0730-0740.MP4"
    #jsonA_path = "output/tracking_results/CameraA_20211118_0730-0740.json"
    #jsonB_path = "output/tracking_results/CameraB_20211118_0730-0740.json"
    camA_reid_dir = "mask_utils/camA_reid.yaml"
    camB_reid_dir = "mask_utils/camB_reid.yaml"
    
    # System setup
    camA_file  = file_loader.FileLoader( camA_path )
    camB_file  = file_loader.FileLoader( camB_path )
    with open(f'{jsonA_path}') as f:
        jsonA_data = json.load(f)
    with open(f'{jsonB_path}') as f:
        jsonB_data = json.load(f)

    camA_masked = mask_area.MaskArea( camA_reid_dir )
    camB_masked = mask_area.MaskArea( camB_reid_dir )
    filename    = jsonB_path.split("/")[-1].split('.')[0]

    # Evaluation_flag
    evaluate_mode  = True
    evaluation_dir = "reid_accuracy/evaluation_"
    evaluaton_gen  = REID_Image_Generator( evaluation_dir, evaluate_mode )

    video_reid_system = reid_system.Reid_System( camA_file.movieLoader, camB_file.movieLoader, jsonA_data, jsonB_data, camA_masked, camB_masked, filename, evaluaton_gen )

if __name__ == '__main__':
    main()
    os._exit(0)

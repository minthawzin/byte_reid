import glob
import os
import cv2
import argparse
from enum import Enum
import ffmpeg
from datetime import datetime, timedelta

class Camera(Enum):
    CameraA = "A"
    CameraB = "B"
    def __str__(self):
        return self.value

    def get_key(self):
        return self.name

class Date(Enum):
    date20211118 = "day1"
    date20211120 = "day2"
    def __str__(self):
        return self.value

    def get_key(self):
        return self.name

def getArguments():
    args = argparse.ArgumentParser()
    args.add_argument("--vid", default="test_videos/test_1.mp4", help="Path to the source video")
    args.add_argument("--camid", type=Camera, choices=list(Camera), default="A", help="Camera ID (A or B)")
    args.add_argument("--date", type=Date, choices=list(Date), default="day1", help="Date (18 or 20)")
    args.add_argument("--start_time", type=float, help="Start time in minutes")
    args.add_argument("--duration", type=float, help="Duration of cut in minutes")
    args.add_argument("--full", type=int, help="1 for full video")
    arguments = args.parse_args()

    return arguments

def outputFormat(cmd_args, start_time, duration):
    datedata = str(Date.get_key(cmd_args.date)).split("date")[-1]
    morning_default_start = datetime(int(datedata[:4]), int(datedata[4:6]), int(datedata[6:8]), 7)

    actual_start = morning_default_start + timedelta(seconds=start_time)
    end_time = actual_start + timedelta(seconds=duration)
    vid_format_name = (f"Camera{cmd_args.camid}_{datedata}_{actual_start.strftime('%H')}{actual_start.strftime('%M')}-{end_time.strftime('%H')}{end_time.strftime('%M')}.MP4")
    return vid_format_name

def main():
    cmd_args = getArguments()
    vid_info=ffmpeg.probe(cmd_args.vid)
    # float is used to solve invalid literal base 10
    vid_duration = int(float(vid_info['format']['duration']))
    if cmd_args.duration is None and cmd_args.start_time is None:
        start_time = 0
        duration = 60*60 # 1 hour interval
        num_of_videos = int(vid_duration/duration)
        vid_count = 1

        # use full flag to reduce the file size first
        if cmd_args.full == 1:
            duration = vid_duration
            num_of_videos = 1

        for i in range(num_of_videos):
            output_name = outputFormat(cmd_args, start_time, duration)
            os.system(f"ffmpeg -ss {start_time} -hwaccel cuda -i {cmd_args.vid} -t {duration} -c:v h264_nvenc {output_name}")
            start_time += duration
            vid_count += 1
            os.system("")

        if start_time < vid_duration:
            duration = vid_duration - start_time
            output_name = outputFormat(cmd_args, start_time, duration)
            os.system(f"ffmpeg -ss {start_time} -hwaccel cuda -i {cmd_args.vid} -t {duration} -c:v h264_nvenc {output_name}")
            pass

    else:
        start_time = int(cmd_args.start_time * 60)
        duration = int(cmd_args.duration * 60)
        output_name = outputFormat(cmd_args, start_time, duration)
        os.system(f"ffmpeg -ss {start_time} -hwaccel cuda -i {cmd_args.vid} -t {duration} -c:v h264_nvenc {output_name}")
        pass

if __name__ == '__main__':
    main()
    os._exit(0)

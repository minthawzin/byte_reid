import json
import cv2
import glob
import os
import argparse

def getArguments():
    args = argparse.ArgumentParser()
    args.add_argument("--vid", required=True, help="Path to the video file")
    args.add_argument('--json', required=True, help="Path to the json file")
    arguments = args.parse_args()

    return arguments

def main():
    cmd_args = getArguments()
    video_cap = cv2.VideoCapture(cmd_args.vid)
    filename = cmd_args.vid.split('/')[-1]
    frame_id = 0
    start_index = 0
    with open(f'{cmd_args.json}') as f:
        json_data = json.load(f)

    eof_json = False
    fourcc = cv2.cv2.VideoWriter_fourcc('M','P','4','V')
    out = cv2.VideoWriter(f'output.MP4', fourcc, 30.0, (1920,1080))
    while(video_cap.isOpened()):
        ret, vid_frame = video_cap.read()
        if ret is True:
            for i in range(len(json_data['timelines'])):
                curr_data = json_data['timelines'][i]
                for frame in curr_data['frames']:
                    if frame['frame'] == frame_id:
                        bbox = frame['bbox']
                        bbox = [int(i) for i in bbox]
                        bicycle = curr_data['bicycle']
                        area = frame['area']
                        bIOU = str(frame['bytetrack_IOU'])
                        if bicycle == 1:
                            cv2.putText(vid_frame, "bicycle", (bbox[0], bbox[1] - 40), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0), 2)
                        #cv2.putText(vid_frame, bIOU, (bbox[0], bbox[1] - 60), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0,255,0), 2)

                        cv2.putText(vid_frame, str(frame['id']), (bbox[0], bbox[1] - 10), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0,255,0), 2)
                        cv2.rectangle(vid_frame, (bbox[0],bbox[1]), (bbox[2], bbox[3]), (0,255,0), 1)

            frame_id += 1
            out.write(vid_frame)
            resize_frame = cv2.resize(vid_frame, (1280, 720))
            cv2.imshow('output', resize_frame)
            key = cv2.waitKey(1) & 0xFF
            if key == ord('q'):
                cv2.destroyAllWindows()
                video_cap.release()
                #out.release()
                os._exit(0)

            elif key == ord('s'):
                cv2.imwrite("output.jpg", vid_frame)

        else:
            break

if __name__ == '__main__':
    main()
    os._exit(0)

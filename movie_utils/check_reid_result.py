import json, cv2, glob, os, sys, tqdm, copy
sys.path.append("./")
from drawing_utils import draw_output
from file_utils import *
from person_reid_utils.cropImg import cropImg

def updateData(json_data, video_frame, frame_number):
    copied_frame = copy.copy(video_frame)
    for i in range(len(json_data['timelines'])):
        curr_data = json_data['timelines'][i]
        for frame in curr_data['frames']:
            if frame['frame'] == frame_number:
                crop_img = cropImg( frame['bbox'], frame['id'], frame['detect_confidence'], copied_frame )
                if frame['id'] > 20000:
                    continue
                cv2.putText( video_frame, str(frame['id']), crop_img.textPosition , cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0,255,0), 2)
                cv2.rectangle( video_frame , crop_img.x1y1, crop_img.x2y2, (0,255,0), 1)

def run( camA, camB, jsonA, jsonB ):
    drawing_   = draw_output.TrackingDraw((5, 250, 247))

    for frame_number in tqdm.tqdm(range( camA.maxMovieFrame )):
        camA.readFrame()
        camB.readFrame()
        updateData( jsonA, camA.currentFrame, frame_number )
        updateData( jsonB, camB.currentFrame, frame_number )
        drawing_.multi_display( camA.currentFrame, camB.currentFrame )
        drawing_.writeToVideo()

    drawing_.releaseVideo()

def main():
    camA_path  = "reid_accuracy/camA/CameraA_20211118_0730-0740_C.MP4"
    camB_path  = "reid_accuracy/camB/CameraB_20211118_0730-0740_C.MP4"
    jsonA_path = "output/tracking_results/CameraA_20211118_0730-0740_C.json"
    jsonB_path = "output/reid_results/CameraB_20211118_0730-0740_C_reid.json"

    # System setup
    camA_file  = file_loader.FileLoader( camA_path )
    camB_file  = file_loader.FileLoader( camB_path )
    with open(f'{jsonA_path}') as f:
        jsonA_data = json.load(f)
    with open(f'{jsonB_path}') as f:
        jsonB_data = json.load(f)

    run( camA_file.movieLoader, camB_file.movieLoader, jsonA_data, jsonB_data )

if __name__ == '__main__':
    main()
    os._exit(0)

import json
import cv2
import glob
import os
import argparse

def getArguments():
    args = argparse.ArgumentParser()
    args.add_argument("--vid", required=True, help="Path to the video file")
    args.add_argument('--json', required=True, help="Path to the json file")
    arguments = args.parse_args()

    return arguments

def main():
    cmd_args = getArguments()
    video_cap = cv2.VideoCapture(cmd_args.vid)

    frame_id = 0
    start_index = 0
    with open(f'{cmd_args.json}') as f:
        json_data = json.load(f)

    eof_json = False
    while(video_cap.isOpened()):
        ret, vid_frame = video_cap.read()
        if ret is True:
            if eof_json == False:
                for i in range(start_index, len(json_data['results'])):
                    curr_data = json_data['results'][i]
                    if curr_data['frame'] > frame_id:
                        start_index = i
                        break
                    else:
                        bbox = curr_data['bbox']
                        category = curr_data['category']
                        if category == 'person':
                            cv2.rectangle(vid_frame, (bbox[0],bbox[1]), (bbox[2], bbox[3]), (0,255,0), 2)
                        elif category == 'bicycle':
                            cv2.rectangle(vid_frame, (bbox[0],bbox[1]), (bbox[2], bbox[3]), (255,0,0), 2)
                        else:
                            cv2.rectangle(vid_frame, (bbox[0],bbox[1]), (bbox[2], bbox[3]), (0,0,255), 2)

                        if i+1 == len(json_data['results']):
                            eof_json = True
                            break
            frame_id += 1
            resize_frame = cv2.resize(vid_frame, (1280, 720))
            cv2.imshow('output', resize_frame)
            key = cv2.waitKey(0) & 0xFF
            if key == ord('q'):
                cv2.destroyAllWindows()
                video_cap.release()
                os._exit(0)
            elif key == ord('s'):
                cv2.imwrite("output.jpg", vid_frame)

        else:
            break

if __name__ == '__main__':
    main()
    os._exit(0)

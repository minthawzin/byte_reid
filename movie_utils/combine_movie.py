import ffmpeg
import glob
import os

list_of_videos_to_combine_in_order = [
'test_videos/day2/camB/CameraB_20211120_0700-0944.MP4',
'test_videos/day2/camB/CameraB_20211120_0945-1229.MP4',
'test_videos/day2/camB/CameraB_20211120_1229-1514.MP4',
'test_videos/day2/camB/CameraB_20211120_1514-1759.MP4',
'test_videos/day2/camB/CameraB_20211120_1759-1902.MP4'
]

with open('combined_list.txt', 'w') as txt_file:
    for vid in list_of_videos_to_combine_in_order:
        txt_file.writelines(f"file {vid}\n")

os.system(f"ffmpeg -f concat -i combined_list.txt -c copy combined_out.mp4")

os.remove('combined_list.txt')

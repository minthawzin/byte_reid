import cv2

class MovieLoader:

    @property
    def movieDir( self ):
        return self.__movieDir

    @property
    def cv2Movie( self ):
        return self.__cv2Movie

    @property
    def maxMovieFrame( self ):
        max_frames = int(self.cv2Movie.get(cv2.CAP_PROP_FRAME_COUNT))
        return max_frames

    @property
    def currentFrame( self ):
        return self.__frame

    @property
    def frameId( self ):
        return self.__frame_id

    @property
    def movieName( self ):
        return self.movieDir.split('/')[-1].split('.')[0]

    @property
    def FPS( self ):
        fps = self.cv2Movie.get(cv2.CAP_PROP_FPS)
        return fps

    def __init__( self, movieDir ):
        self.__movieDir = movieDir
        self.__cv2Movie = cv2.VideoCapture( self.movieDir )
        self.__frame = None
        self.__frame_id = -1

    def readFrame( self ):
        res, self.__frame = self.cv2Movie.read()
        self.__frame_id += 1

import os
from file_utils import movie_loader

class FileLoader:

    @property
    def filename( self ):
        return self.__filename

    @property
    def fileExtension( self ):
        extension =  os.path.splitext(self.filename)[1]
        return extension

    @property
    def validFileTypes( self ):
        validFileTypes = [ ".MP4", ".mp4", ".AVI", ".avi" ]
        return validFileTypes

    @property
    def movieLoader( self ):
        return self.__movieLoader

    @property
    def movieName( self ):
        return self.filename.split('/')[-1].split('.')[0]

    def __init__( self, filename ):
        self.__filename = filename
        self.checkValidFileTypes()
        self.__movieLoader = movie_loader.MovieLoader( self.filename )

    def checkValidFileTypes( self ):
        if self.fileExtension not in self.validFileTypes:
            print( "File Type is not Valid" )
            os._exit(0)

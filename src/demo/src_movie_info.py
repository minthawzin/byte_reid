'''
@file src_movie_info.py

@author Min Thaw Zin / created on 2022/1/28
@copyright 2022 GlobalWalkers.inc. All rights reserved.
'''

import cv2

class SrcMovieInfo():

    @property
    def maxFrameNo( self ):
        '''
            Get the max frame number of the source movie

            @return max frame number
        '''
        max_frame_number = int(self.videoCaptureObject.get(cv2.CAP_PROP_FRAME_COUNT))
        return max_frame_number

    @property
    def getFrame( self ):
        '''
            Get frame of video Object

            @return frame object of video or None if no frame
        '''
        ret, frame = self.videoCaptureObject.read()
        if ret is False:
            return None
        else:
            return frame

    @property
    def videoCaptureObject( self ):
        '''
            Get video capture Object

            @return cv2.videoCapture()
        '''
        return self.__videoCaptureObject

    @property
    def moviePath( self ):
        '''
            Get movie Path

            @return movie path
        '''
        return self.__moviePath

    def __init__( self, movie_path ):
        '''
        Movie Constructor Class
        '''
        super().__init__()
        self.__moviePath = movie_path
        self.__videoCaptureObject = cv2.VideoCapture(self.moviePath)

'''
@file tracking_movie_generator.py

@author Min Thaw Zin / created on 2022/1/28
@copyright 2022 GlobalWalkers.inc. All rights reserved.
'''

from db.dataset import TrackingDataset
from demo.src_movie_info import SrcMovieInfo
from demo.dst_movie_info import DstMovieInfo

class TrackingMovieGenerator( object ):
    '''
        Demo Movie Geneerator Class
    '''

    @property
    def jsonInfo( self ):
        return self.__jsonInfo

    @property
    def dataset( self ):
        return self.__dataset

    @property
    def srcMovie( self ):
        return self.__src_movie

    @property
    def dstMovie( self ):
        return self.__dst_movie

    def __init__( self, jsonInfo ):
        '''
            Constructor
        '''
        super( TrackingMovieGenerator, self ).__init__()
        self.__jsonInfo = jsonInfo
        self.__dataset = TrackingDataset(
            jsonInfo.camera,
            jsonInfo.dateStr,
            jsonInfo.startTimeStr,
            jsonInfo.endTimeStr,
            jsonInfo.jsonPath
        )
        self.__src_movie = SrcMovieInfo(self.jsonInfo.lookupMoviePath())

    def generate( self, dstMovPath ):
        '''
            Generate Demo Movie
        '''
        print( "[TrackinMovieGenerator]: generate start" )
        print( "\tsrc movie path: %s" % self.jsonInfo.lookupMoviePath() )
        print( "\tdst movie path: %s" % dstMovPath )
        self.__dst_movie = DstMovieInfo(dstMovPath)

        self.dataset.restore()

        for frameNo in range( self.srcMovie.maxFrameNo ):
            self.dstMovie.drawSingleFrame(self.srcMovie.getFrame, self.dataset.getDataList( frameNo ))
            """
            for data in self.dataset.getDataList( frameNo ):
                print(
                    "[Cam%s::%4d] x1:%.2f y1:%.2f x2:%.2f y2:%.2f id:%s" % (
                        data.camera, data.frameNo, data.x1, data.y1, data.x2, data.y2, data.oid
                    )
                )
            """

        print( "[TrackinMovieGenerator]: generate completed" )

'''
@file dst_movie_info.py

@author Min Thaw Zin / created on 2022/1/28
@copyright 2022 GlobalWalkers.inc. All rights reserved.
'''

import os, cv2, numpy

class DstMovieInfo():

    @property
    def dstMoviePath( self ):
        return self.__dstMoviePath

    @property
    def bboxColor( self ):
        color = (5, 250, 247)
        return color

    @property
    def outputMovie( self ):
        return self.__output_video

    @property
    def outputWidth( self ):
        return self.__width

    @property
    def outputHeight( self ):
        return self.__height

    @property
    def outputReidHeight( self ):
        return self.__reid_height

    def checkArea( self, area_flag ):
        if area_flag == 'NA':
            area_flag = ""
        else:
            area_flag = area_flag
        return area_flag

    def checkBus( self, bus_flag ):
        if bus_flag == 0:
            bus_flag = ""
        else:
            bus_flag = "bus"
        return bus_flag

    def checkBicycle( self, bicycle_flag ):
        if bicycle_flag == False:
            bicycle_flag = ""
        else:
            bicycle_flag = "bicycle"
        return bicycle_flag

    def outputAttributes(self, bus_flag, area_flag, bicycle_flag):
        area_flag = self.checkArea(area_flag)
        bus_flag = self.checkBus(bus_flag)
        bicycle_flag = self.checkBicycle(bicycle_flag)

        display_format = f"{area_flag} {bicycle_flag} {bus_flag}"
        return display_format


    def __init__(self, dstMoviePath, reid_flag=False):
        '''
            Constructor for destination movie path

        '''
        super().__init__()
        self.__dstMoviePath = dstMoviePath
        self.__fourcc = cv2.VideoWriter_fourcc('M','P','4','V')
        self.__height = 1080
        self.__width = 1920
        self.__reid_height = 720
        if reid_flag == False:
            self.__output_video = cv2.VideoWriter(self.dstMoviePath, self.__fourcc, 30.0, (self.outputWidth, self.outputHeight))
        else:
            self.__reid_video = cv2.VideoWriter(self.dstMoviePath, self.__fourcc, 30.0, (self.outputWidth, self.outputReidHeight))

    def drawSingleFrame(self, frame, data_list):
        '''
            Draw and save output into the destination movie for tracking

        '''
        if frame is not None:
            for data in data_list:
                cv2.rectangle(frame, (int(data.x1), int(data.y1)), (int(data.x2), int(data.y2)), \
                                        self.bboxColor, thickness=2)
                data_attributes = self.outputAttributes(data.bus, data.area, data.bicycleFlag)
                cv2.putText(frame, f"{str(data.oid)}", (int(data.x1), int(data.y1 - 10)), cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.95, color=self.bboxColor, thickness=2)
                cv2.putText(frame, f"{str(data_attributes)}", (int(data.x1), int(data.y1 - 40)), cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.6, color=self.bboxColor, thickness=2)


            self.outputMovie.write(frame)
        # get of frame
        else:
            self.outputMovie.release()

    def drawDoubleFrame(self, frameA, frameB, data_list):
        '''
            Draw and save output into the destination movie for re identification

        '''
        if frameA is not None and frameB is not None:
            for data in data_list:
                if data.camera == 'A':
                    cv2.rectangle(frameA, (int(data.x1), int(data.y1)), (int(data.x2), int(data.y2)), \
                                            self.bboxColor, thickness=2)
                    data_attributes = self.outputAttributes(data.bus, data.area, data.bicycleFlag)
                    cv2.putText(frameA, f"{str(data.oid)}", (int(data.x1), int(data.y1 - 10)), cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.95, color=self.bboxColor, thickness=2)
                    cv2.putText(frameA, f"{str(data_attributes)}", (int(data.x1), int(data.y1 - 50)), cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.65, color=self.bboxColor, thickness=2)
                elif data.camera == 'B':
                    cv2.rectangle(frameB, (int(data.x1), int(data.y1)), (int(data.x2), int(data.y2)), \
                                            self.bboxColor, thickness=2)
                    data_attributes = self.outputAttributes(data.bus, data.area, data.bicycleFlag)
                    cv2.putText(frameB, f"{str(data.oid)}", (int(data.x1), int(data.y1 - 10)), cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.95, color=self.bboxColor, thickness=2)
                    cv2.putText(frameB, f"{str(data_attributes)}", (int(data.x1), int(data.y1 - 50)), cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.65, color=self.bboxColor, thickness=2)

            output_frame = self.combineFrames(frameA, frameB)
            self.__reid_video.write(output_frame)


        # get of frame
        else:
            self.__reid_video.release()

    def combineFrames(self, frameA, frameB):
        resize_width = int(self.outputWidth / 2)
        resize_frame_A = cv2.resize(frameA, (resize_width, self.outputReidHeight))
        resize_frame_B = cv2.resize(frameB, (resize_width, self.outputReidHeight))
        combined_resized_frame = numpy.concatenate((resize_frame_A, resize_frame_B), axis=1)
        return combined_resized_frame

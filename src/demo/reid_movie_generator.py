'''
@file reid_movie_generator.py

@author Min Thaw Zin / created on 2022/1/28
@copyright 2022 GlobalWalkers.inc. All rights reserved.
'''

from db.dataset import TrackingDataset
from demo.src_movie_info import SrcMovieInfo
from demo.dst_movie_info import DstMovieInfo

class ReidMovieGenerator( object ):
    '''
        Demo Movie Geneerator Class
    '''

    FRAME_WIDTH = 1920

    @property
    def jsonInfoMap( self ):
        return self.__jsonInfoMap

    @property
    def datasetMap( self ):
        return self.__datasetMap

    @property
    def srcCamA( self ):
        return self.__srcCamA

    @property
    def srcCamB( self ):
        return self.__srcCamB

    @property
    def dstMovie( self ):
        return self.__dst_movie

    def __init__( self, jsonSet ):
        '''
            Constructor
        '''
        super( ReidMovieGenerator, self ).__init__()
        self.__jsonInfoMap = {}
        self.__datasetMap = {}
        for camera, jsonInfo in jsonSet.eachInfo():
            self.__jsonInfoMap[ camera ] = jsonInfo
            self.__datasetMap[ camera ] = TrackingDataset(
                jsonInfo.camera,
                jsonInfo.dateStr,
                jsonInfo.startTimeStr,
                jsonInfo.endTimeStr,
                jsonInfo.jsonPath
            )

        self.__srcCamA = SrcMovieInfo(self.jsonInfoMap[ 'A' ].lookupMoviePath())
        self.__srcCamB = SrcMovieInfo(self.jsonInfoMap[ 'B' ].lookupMoviePath())

    def __restore( self ):
        '''
            Restore Dataset
        '''
        for dataset in list( self.datasetMap.values() ):
            dataset.restore()

    def __eachData( self, frameNo ):
        '''
            Get Data List (Extension)
        '''
        for camera, dataset in self.datasetMap.items():
            offsetX = 0
            if camera == 'A':
                pass
                #offsetX = ReidMovieGenerator.FRAME_WIDTH
            for data in dataset.getDataList( frameNo, offsetX = offsetX ):
                yield data

    def generate( self, dstMovPath ):
        '''
            Generate Demo Movie
        '''
        print( "[ReidMovieGenerator]: generate start" )
        print( "\tsrc movie path:" )
        print( "\t\t%s" % self.jsonInfoMap[ 'A' ].lookupMoviePath() )
        print( "\t\t%s" % self.jsonInfoMap[ 'B' ].lookupMoviePath() )
        print( "\tdst movie path: %s" % dstMovPath )
        self.__dst_movie = DstMovieInfo(dstMovPath, reid_flag=True)

        self.__restore()

        for frameNo in range( self.srcCamA.maxFrameNo ):
            self.dstMovie.drawDoubleFrame(self.srcCamA.getFrame, self.srcCamB.getFrame, self.__eachData( frameNo ))
            """
            for data in self.__eachData( frameNo ):
                print(
                    "[Cam%s::%4d] x1:%.2f y1:%.2f x2:%.2f y2:%.2f id:%s" % (
                        data.camera, data.frameNo, data.x1, data.y1, data.x2, data.y2, data.oid
                    )
                )
            """

        print( "[ReidMovieGenerator]: generate completed" )

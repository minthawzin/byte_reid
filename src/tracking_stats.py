'''
@file tracking_stats.py

@author ssugino / created on 2022/1/10
@copyright 2022 GlobalWalkers.inc. All rights reserved.
'''
import os, sys
HTACH_TECHNOLOGY_SRC_PATH = os.path.dirname( os.path.dirname( __file__ ) )
sys.path.append( HTACH_TECHNOLOGY_SRC_PATH )

from db.data_file_loader import DataFileLoader
from db.database import TrackingDB

#------------------------------------------------------------------------------
# Functions
#------------------------------------------------------------------------------

def eachCameraPair( db ):
    '''
        Evaluate Camera Pair Dataset

        @param db: TrackingDB
        @yield datasetA: TrackingDataset of CameraA
        @yield datasetB: TrackingDataset of CameraB
    '''
    camAList = list( db.lookupDataset( expCamera="CameraA" ) )
    for datasetA in camAList:
        camBList = list (
            db.lookupDataset( 
                expCamera    = "CameraB",
                expDate      = datasetA.date,
                expStartTime = datasetA.startTime,
                expEndTime   = datasetA.endTime
            )
        )

        if 1 < len( camBList ):
            raise AttributeError( "Duplicate Dataset Found (Target: %s)" % datasetA.did )
        if len( camBList ) < 1:
            raise AttributeError( "Pair Camera Not Found (Target: %s)" % datasetB.did );
        datasetB = camBList[ 0 ]

        yield datasetA, datasetB

def printStats( db ):
    '''
        Print statistice to standard out
        Format: <date>,<startTime>,<endTime>,
                <Object Count(A)>,<Average Frame Count(A)>,<Average Detect Confidence(A)>,<Average Tracking Confidence(A)>,
                <Object Count(A)>,<Average Frame Count(B)>,<Average Detect Confidence(B)>,<Average Tracking Confidence(B)>,

        @param db: TrackingDB
    '''
    for datasetA, datasetB in eachCameraPair( db ):

        datasetA.restore()
        datasetB.restore()

        print( 
            "%s,%s,%s,%s,%s" % (
                datasetA.date, datasetA.startTime, datasetA.endTime,
                datasetA, datasetB
            )
        )

def makeDB( dirPath ):
    '''
        Make Tracking Database from File

        @param dirPath: Directory Path of saved Json File
        @return db: TrackingDB instance
    '''
    loader = DataFileLoader()
    db = TrackingDB()
    for dataset in loader.load( dirPath ):
        db.append( dataset )
    return db

#------------------------------------------------------------------------------
# Main Routine
#------------------------------------------------------------------------------
if __name__ == '__main__':

    dirPath = "/Users/ssugino/Downloads/Intage/Nagoya/Report/GW_20220107/JsonData"
    
    db = makeDB( dirPath )
    printStats( db )

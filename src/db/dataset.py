'''
@file dataset.py
 
@author ssugino / created on 2022/1/10
@copyright 2022 GlobalWalkers.inc. All rights reserved.
'''
import os, statistics, codecs, json
from db.timeline import Timeline

class TrackingDataset( object ):
    '''
        Tracking Dataset Class
    '''

    @property
    def jsonPath( self ):
        return self.__jsonPath
    @property
    def jsonName( self ):
        return os.path.basename( self.__jsonPath )

    @property
    def camera( self ):
        return self.__camera
    @property
    def date( self ):
        return self.__date
    @property
    def startTime( self ):
        return self.__startTime
    @property
    def endTime( self ):
        return self.__endTime
    @property
    def did( self ):
        return self.__datasetID

    @property
    def oidList( self ):
        oidList = list( self.__timelines.keys() )
        oidList.sort()
        return oidList
    @property
    def objectCount( self ):
        return len( self.oidList )
    @property
    def minOID( self ):
        return min( self.oidList )
    @property
    def maxOID( self ):
        return max( self.oidList )

    def __init__( self, camera, date, startTime, endTime, jsonPath = None ):
        '''
            Constructor
            
            @param camera: Camera Name
            @param date: Shooting Date
            @param startTime; Shooting Start Time
            @param endTime: Shooting End Time
        '''
        super( TrackingDataset, self ).__init__()
        self.__camera = camera
        self.__date = date
        self.__startTime = startTime
        self.__endTime = endTime
        self.__datasetID = "%s_%s_%s-%s" % ( camera, date, startTime, endTime )
        self.__timelines = {}
        self.__jsonPath = jsonPath

    def restore( self ):
        '''
            Restore Data from Json File

        '''
        if self.__jsonPath is None:
            raise AttributeError( "Not setting Json File Path" )

        with codecs.open( self.__jsonPath, 'r', encoding='utf-8' ) as fd:
            jsonData = json.load( fd )
            for timelineData in jsonData[ 'timelines' ]:
                timeline = Timeline(
                    timelineData[ 'category' ],
                    timelineData[ 'id' ],
                    timelineData[ 'bicycle' ] == 1,
                    timelineData
                ) 
                timeline.setConf( self.__camera, self.__date, self.__startTime )
                timeline.lookupArea()
                self.append( timeline )

    def lookupArea( self ):
        '''
            All Timeline Lookup Area
        '''
        for timeline in list( self.__timelines.values() ):
            timeline.lookupArea()

    def append( self, timeline ):
        '''
            Append Timeline Data
            
            @param timeline: Timeline Data
        '''
        if timeline is None:
            raise AttributeError( "None is invalid timeline data" )
        self.__timelines[ timeline.oid ] = timeline

    def isCamera( self, expCamera ):
        '''
            Evaluate Camera

            @param expDate: Expected Camera Type
            @return True = match / False = unmatch
        '''
        return self.__camera == expCamera

    def isDate( self, expDate ):
        '''
            Evaluate Date

            @param expDate: Expected Date
            @return True = match / False = unmatch
        '''
        return self.__date == expDate

    def isStartTime( self, expStartTime ):
        '''
            Evaluate Start Time

            @param expStartTime: Expected Start Time
            @return True = match / False = unmatch
        '''
        return self.__startTime == expStartTime

    def isEndTime( self, expEndTime ):
        '''
            Evaluate End Time

            @param expEndTime: Expected End Time
            @return True = match / False = unmatch
        '''
        return self.__endTime == expEndTime

    def eachTimeline( self ):
        '''
            Evaluate each Timeline

            @yield timeline: Timeline instance
        '''
        for oid in self.oidList:
            yield self.__timelines[ oid ]

    def getDataList( self, frameNo, offsetX=0, offsetY=0 ):
        '''
            Get Object Data List
        '''
        dataList = []
        for timeline in list( self.__timelines.values() ):

            if not timeline.isInclude( frameNo ):
                continue

            try:
                data = timeline.getData( frameNo, offsetX=offsetX, offsetY=offsetY )
                dataList.append( data )
            except:
                # ignore
                continue

        return dataList

    def merge( self, other ):
        '''
            Merge Dataset
            
            @param other: Other Timeline
        '''
        print( "[TrackingDataset]: ----- Merge Start (Base: Camera%s) -----" % self.camera )
        print( "\t* from: %s" % other.jsonName )
        print( "\t* to  : %s" % self.jsonName )

        for fromTimeline in other.eachTimeline():
            if fromTimeline.oid in self.__timelines:
                toTimeline = self.__timelines[ fromTimeline.oid ]
                toTimeline.merge( fromTimeline ) 
            else:
                self.__timelines[ fromTimeline.oid ] = fromTimeline

        for oid, timeline in self.__timelines.items():
            timeline.lookupArea()

        print( "[TrackingDataset]: ----- Merge End -----" );

    def averageFrameCount( self ):
        '''
            Get Average Frame Count of Timeline

            @return float: Average Frame Count of Timeline
        '''
        frameCntList = list(
            map(
                lambda e: e.frameCount,
                list( self.__timelines.values() )
            )
        )
        return statistics.mean( frameCntList )

    def averageDetectConfidence( self ):
        '''
            Get Average Detect Confidence of Timeline

            @return float: Average Detect Confidence of Timeline
        '''
        avgDetectConfList = list(
            map(
                lambda e: e.averageDetectConfidence(),
                list( self.__timelines.values() )
            )
        )
        return statistics.mean( avgDetectConfList )

    def averageTrackingConfidence( self ):
        '''
            Get Average Tracking Confidence of Timeline

            @return float: Average Tracking Confidence of Timeline
        '''
        avgTrackConfList = list(
            map(
                lambda e: e.averageTrackingConfidence(),
                list( self.__timelines.values() )
            )
        )
        return statistics.mean( avgTrackConfList )

    def __repr__( self ):
        '''
            Get String of this instance
            @note: format = "<object count>,<average frame count>,<average detect confidence>,<average tracking confidence>"
            @return String: String of this instance
        '''
        return "%d,%.2f,%.2f,%.2f" % (
            self.objectCount,
            self.averageFrameCount(),
            self.averageDetectConfidence(),
            self.averageTrackingConfidence()
        )

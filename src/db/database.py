'''
@file database.py
 
@author ssugino / created on 2022/1/10
@copyright 2022 GlobalWalkers.inc. All rights reserved.
'''

class TrackingDB( object ):
    '''
        Tracking Database Class
    '''

    @property
    def didList( self ):
        didList = list( self.__datasetMap.keys() )
        didList.sort()
        return didList

    @property
    def datasetCount( self ):
        return len( didList )

    def __init__( self ):
        '''
            Constructor
        '''
        super( TrackingDB, self ).__init__()
        self.__datasetMap = {}

    def append( self, dataset ):
        '''
            Append Dataset

            @param dataset: Dataset instance
        '''
        if dataset is None:
            raise AttributeError( "None is invalid Dataset" );
        self.__datasetMap[ dataset.did ] = dataset

    def eachDataset( self ):
        '''
            Eveluate Each Dataset
            
            @yield dataset: Dataset instance
        '''
        for did in self.didList:
            yield self.__datasetMap[ did ]

    def lookupDataset( self, expCamera = None, expDate = None, expStartTime = None, expEndTime = None ):
        '''
            Lookup Dataset
            
            @param expCamera: Expected Camera
            @param expDate: Expected Date
            @param expStartTime: Expected Start Time
            @param expEndTime: Expected End Time
            @yield candidate: Matched TrackingDataset
        '''
        for candidate in self.eachDataset():

            if ( expCamera is not None ) and ( not candidate.isCamera( expCamera ) ):
                continue
            if ( expDate is not None ) and ( not candidate.isDate( expDate ) ):
                continue
            if ( expStartTime is not None ) and ( not candidate.isStartTime( expStartTime ) ):
                continue
            if ( expEndTime is not None ) and ( not candidate.isEndTime( expEndTime ) ):
                continue

            yield candidate

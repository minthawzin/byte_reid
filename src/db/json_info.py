'''
@file json_info.py
 
@author ssugino / created on 2022/1/24
@copyright 2022 GlobalWalkers.inc. All rights reserved.
'''
import os, re, glob, datetime

class JsonInfo( object ):
    '''
        Json Information Class (per Json File)
    '''

    @property
    def jsonDir( self ): return self.__jsonDir
    @property
    def jsonName( self ): return self.__jsonName
    @property
    def jsonPath( self ): return self.__jsonPath
    @property
    def movName( self ): return self.__movName
    @property
    def camera( self ): return self.__cameraID
    @property
    def dateStr( self ): return self.__dateStr
    @property
    def startTimeStr( self ): return self.__startTimeStr
    @property
    def endTimeStr( self ): return self.__endTimeStr
    @property
    def baseDatetime( self ): return self.__baseDatetime

    def __init__( self, jsonPath ):
        '''
            Constructor
        '''
        super( JsonInfo, self ).__init__()
        self.__jsonPath = jsonPath
        self.__jsonDir  = os.path.dirname( jsonPath )
        self.__jsonName = os.path.basename( jsonPath )
        self.__movName = self.__jsonName.replace( ".json", ".MP4" )

        result = re.match( 
            r"Camera(?P<camera>.+)_(?P<date>\d+)_(?P<startTime>\d+)-(?P<endTime>\d+)(.*)\.json", 
            self.__jsonName
        )
        self.__cameraID = result[ 'camera' ]
        self.__dateStr  = result[ 'date' ]
        self.__startTimeStr = result[ 'startTime' ]
        self.__endTimeStr   = result[ 'endTime' ]
        self.__makeBaseDatetime()

    def __makeBaseDatetime( self ):
        '''
            Make base datetime from datetime string
        '''
        self.__baseDatetime = datetime.datetime.strptime( 
            "%s %s" % ( self.__dateStr, self.__startTimeStr ), 
            '%Y%m%d %H%M'  
        )

        if self.__dateStr == '20211118':
            adjustMsec = int( ( 92 / 30 ) * 1000 )
            deltaTime = datetime.timedelta( milliseconds = adjustMsec )
            self.__baseDatetime = self.__baseDatetime + deltaTime

        if self.__dateStr == '20211120':
            adjustMsec = int( ( 7 / 30 ) * 1000 )
            deltaTime = datetime.timedelta( milliseconds = adjustMsec )
            self.__baseDatetime = self.__baseDatetime - deltaTime

    def lookup( self, cameraType, ext = "" ):
        '''
            Lookup json that specified cameraType

            @note for the pair json lookup
        '''
        expJsonName = "Camera%s_%s_%s-%s%s.json" % (
            cameraType, self.dateStr, self.startTimeStr, self.endTimeStr, ext
        )
        expJsonPath = os.path.join( self.jsonDir, expJsonName )
        if not os.path.exists( expJsonPath ):
            raise AttributeError( "[WARN]: %s is not found" % expJsonPath )
        return JsonInfo( expJsonPath )

    def lookupMoviePath( self ):
        '''
            Lookup for the cropping movie path
        '''
        expMovName = "Camera%s_%s_%s-%s.MP4" % (
            self.camera, self.dateStr, self.startTimeStr, self.endTimeStr
        )
        expMovPath = os.path.join( self.jsonDir, expMovName )
        if not os.path.exists( expMovPath ):
            raise AttributeError( "[WARN]: %s is not found" % expMovPath );
        return expMovPath

    def __repr__( self ):
        '''
            Return this instance string
        '''
        return os.path.basename( self.jsonName )
        # return 'Camera%s %s' % (
        #     self.camera,
        #     self.baseDatetime.strftime( '%Y/%m/%d %H:%M' )
        # )

class JsonSet( object ):
    '''
        Json Information Set class
        Management of Json Information Set (Camera A/B)
    '''

    @property
    def jsonDir( self ):
        return self.__jsonMap[ 'A' ].jsonDir
    @property
    def dateStr( self ):
        return self.__jsonMap[ 'A' ].dateStr
    @property
    def startTimeStr( self ):
        return self.__jsonMap[ 'A' ].startTimeStr
    @property
    def endTimeStr( self ):
        return self.__jsonMap[ 'A' ].endTimeStr

    def __init__( self, camAJsonPath, camBExt ):
        '''
            Constructor
        '''
        super( JsonSet, self ).__init__()
        
        jsonInfoA = None
        jsonInfoB = None

        try:
            jsonInfoA = JsonInfo( camAJsonPath )
            jsonInfoB = jsonInfoA.lookup( 'B', ext = camBExt )
        except AttributeError as e:
            print( e )

        self.__jsonMap = {}
        if jsonInfoA is not None:
            self.__jsonMap[ jsonInfoA.camera ] = jsonInfoA
        if jsonInfoB is not None:
            self.__jsonMap[ jsonInfoB.camera ] = jsonInfoB

    def eachInfo( self ):
        '''
            Eveluate each Json Information ( each Camera )
        '''
        cameraList = list( self.__jsonMap.keys() )
        cameraList.sort()
        for camera in cameraList:
            yield camera, self.__jsonMap[ camera ]

    def getJsonInfo( self, camera ):
        '''
            Get Json Information that specified Camera
        '''
        if camera not in self.__jsonMap:
            raise AttributeError( "[ERROR]: Camera %s json file is not found" % camera )
        return self.__jsonMap[ camera ]

class JsonSetList( object ):
    '''
        Json Information (Pair) List
    '''

    def __init__( self, dirPath, camBExt="" ):
        '''
            Constructor
        '''
        super( JsonSetList, self ).__init__()
        
        jsonList = glob.glob( "%s/**/CameraA_*.json" % dirPath, recursive=True )
        jsonList.sort()

        self.__setlist = []
        for jsonPath in jsonList:
            self.__setlist.append( JsonSet( jsonPath, camBExt ) )

    def eachSet( self ):
        '''
            Evaluate each Json Information Pair
        '''
        for jsonSet in self.__setlist:
            yield jsonSet

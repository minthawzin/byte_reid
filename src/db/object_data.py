'''
@file object_data.py

@author ssugino / created on 2022/1/10
@copyright 2022 GlobalWalkers.inc. All rights reserved.
'''
import datetime

class ObjectData( object ):
    '''
        Object Data Class ( per Frame )
    '''

    @property
    def camera( self ):
        return self.__camera
    @property
    def x1( self ):
        return self.__coordinate[ 0 ] + self.offsetX
    @property
    def y1( self ):
        return self.__coordinate[ 1 ] + self.offsetY
    @property
    def x2( self ):
        return self.__coordinate[ 2 ] + self.offsetX
    @property
    def y2( self ):
        return self.__coordinate[ 3 ] + self.offsetY
    @property
    def width( self ):
        return self.x2 - self.x1
    @property
    def height( self ):
        return self.y2 - self.y1
    @property
    def offsetX( self ):
        return self.__offsetX
    @offsetX.setter
    def offsetX( self, offset ):
        self.__offsetX = offset
    @property
    def offsetY( self ):
        return self.__offsetY
    @offsetY.setter
    def offsetY( self, offset ):
        self.__offsetY = offset

    @property
    def frameNo( self ):
        return self.__frame
    @property
    def category( self ):
        return self.__category
    @property
    def preid( self ):
        return self.__preid
    @property
    def oid( self ):
        return self.__oid
    @property
    def dataid( self ):
        return self.__dataID
    @property
    def area( self ):
        return self.__area
    @property
    def bus( self ):
        return self.__busFlag
    @property
    def detectConfidence( self ):
        return self.__detectConfidence
    @property
    def trackingConfidence( self ):
        return self.__trackingConfidence
    @property
    def reidConfidence( self ):
        return self.__reidConfidence

    @property
    def bicycleFlag( self ):
        return self.__bicycleFlag

    def __init__( self, jsonData, bicycleFlag=None):
        '''
            Constructor

            @param jsonData: Object Json Data
        '''
        super( ObjectData, self ).__init__()
        self.__camera     = "A"
        self.__frame      = jsonData[ 'frame' ]
        self.__category   = jsonData[ 'category' ]
        self.__oid        = jsonData[ 'id' ]
        self.__coordinate = jsonData[ 'bbox' ]
        self.__bicycleFlag = bicycleFlag
        self.__offsetX    = 0
        self.__offsetY    = 0
        self.__detectConfidence   = jsonData[ 'detect_confidence' ]
        self.__trackingConfidence = jsonData[ 'tracking_confidence' ]

        self.__dataID = 'None'

        self.__preid = self.__oid
        if 'pre_id' in jsonData:
            self.__preid = jsonData[ 'pre_id' ]

        self.__reidConfidence = self.__trackingConfidence
        if 'reid_confidence' in jsonData:
            self.__reidConfidence = jsonData[ 'reid_confidence' ]

        self.__area = "NA"
        if 'area' in jsonData and jsonData[ 'area' ] != 'NP':
            self.__area = jsonData[ 'area' ]

        self.__busFlag = 0
        if 'bus' in jsonData:
            self.__busFlag = jsonData[ 'bus' ]

    def getDatetime( self, baseDatetime, frameRate = 30 ):
        '''
            Get Datetime String

            @param baseDatetime : start datetime object
            @param frameRate : Frame Rate of Target Movie
        '''
        elapsedMsec = int( ( self.frameNo / frameRate ) * 1000 )
        curDatetime = baseDatetime + datetime.timedelta( milliseconds = elapsedMsec )
        return curDatetime.strftime( '%Y/%m/%d %H:%M:%S.%f' )

    def setConf( self, camera, dateStr, startTimeStr ):
        '''
            Setting Configuration
        '''
        self.__camera = camera
        self.__dataID = "%s%s_%s_%d_%d" % (
            dateStr, startTimeStr, camera, self.oid, self.frameNo
        )
        return self.__dataID

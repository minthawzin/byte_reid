'''
@file timeline.py

@author ssugino / created on 2022/1/10
@copyright 2022 GlobalWalkers.inc. All rights reserved.
'''
import statistics
from db.object_data import ObjectData

class Timeline( object ):

    @property
    def category( self ):
        return self.__category
    @property
    def oid( self ):
        return self.__objectID
    @property
    def bicycleFlag( self ):
        return self.__bicycleFlag
    @property
    def busFlag( self ):
        return self.__busFlag

    @property
    def frameNoList( self ):
        frameNoList = list( self.__frames.keys() )
        frameNoList.sort()
        return frameNoList
    @property
    def frameCount( self ):
        return len( self.frameNoList )
    @property
    def minFrameNo( self ):
        if len( self.__frames ) == 0:
            return -1
        return self.frameNoList[ 0 ]
    @property
    def maxFrameNo( self ):
        if len( self.__frames ) == 0:
            return -1
        return self.frameNoList[ -1 ]

    @property
    def startData( self ):
        return self.__startData
    @property
    def endData( self ):
        return self.__endData
    @property
    def areaStartData( self ):
        return self.__areaStartData
    @property
    def areaEndData( self ):
        return self.__areaEndData

    def __init__( self, category, objectID, bicycleFlag, jsonData = None ):
        '''
            Constructor

            @param jsonData: Timeline Json Data
        '''
        super( Timeline, self ).__init__()
        self.__category = category
        self.__objectID = objectID
        self.__bicycleFlag = bicycleFlag
        self.__busFlag = 0
        self.__frames = {}
        self.__cameraSet = set()

        self.__startData = None
        self.__endData = None
        self.__areaStartData = None
        self.__areaEndData = None

        if jsonData is not None:
            self.__parse( jsonData )

    def __parse( self, jsonData ):
        '''
            Load from jsonData

            @param jsonData: Json Data of Timeline
        '''
        if "frames" not in jsonData:
            raise AttributeError( "Not Found Frame Data" )
        for frameData in jsonData[ 'frames' ]:
            self.__append( ObjectData( frameData, self.bicycleFlag ) )

    def __append( self, objectData ):
        '''
            Append Object Data

            @param objectData: ObjectData instance
        '''
        if objectData is None:
            raise AttributeError( "invalid data append." )
        if objectData.frameNo in self.__frames:
            raise AttributeError( "Duplicate frame data ( frameNo = %d )" % objectData.frameNo )

        if objectData.bus == 1:
            self.__busFlag = 1

        self.__frames[ objectData.frameNo ] = objectData

    def setConf( self, cameraType, dateStr, startTimeStr ):
        '''
            Set Camera Type
        '''
        self.__cameraSet.add( cameraType )
        for frameData in self.eachData():
            frameData.setConf( cameraType, dateStr, startTimeStr )

    def merge( self, other ):
        '''
            Merge Timeline
        '''
        for frameData in other.eachData():
            # @note self timeline is priority high
            if frameData.frameNo in self.__frames:
                continue
            self.__cameraSet.add( frameData.camera )
            if frameData.bus == 1:
                self.__busFlag = 1
            self.__frames[ frameData.frameNo ] = frameData

    def lookupArea( self ):
        '''
            Lookup the Area Configuration
        '''
        frameNoList = self.frameNoList
        frames = list( self.__frames.values() )
        frames.sort( key = lambda e: e.frameNo )

        self.__startData = frames[ 0 ]
        self.__endData   = frames[ -1 ]

        self.__areaStartData = None
        self.__areaEndData = None

        targets = list( filter( lambda e: e.area != 'NA', frames ) )
        if len( targets ) != 0:

            self.__areaStartData = targets[ 0 ]
            self.__areaEndData   = targets[ -1 ]

            # @attention not adopted adjust area analysis when start/end Area is same

            # startArea = self.__areaStartData.area
            # endArea   = self.__areaEndData.area

            # startArea = startArea.replace( 'BA', 'A' )
            # endArea   = endArea.replace( 'BA', 'A' )

            # if startArea == endArea:
            #     diffStart = self.__areaStartData.frameNo - self.__startData.frameNo
            #     diffEnd   = self.__endData.frameNo - self.__areaStartData.frameNo
            #     if diffStart <= diffEnd:
            #         self.__areaEndData = None
            #     else:
            #         self.__areaStartData = None

    def eachData( self ):
        '''
            Evaluate Object Data

            @yeild ObjectData instance (sort by frameNo)
        '''
        for frameNo in self.frameNoList:
            yield self.__frames[ frameNo ]

    def isInclude( self, frameNo ):
        '''
            Check include specified frameNo
            @note please call after lookupArea
        '''
        if self.__startData is None or self.__endData is None:
            raise AttributeError( "[WARN]: Timeline::isInclude call after lookupArea" )

        return (
            self.__startData.frameNo <= frameNo and \
            frameNo <= self.__endData.frameNo
        )

    def getData( self, frameNo, offsetX=0, offsetY=0 ):
        '''
            Get Object Data that specified frameNo
        '''
        if frameNo not in self.__frames:
            raise AttributeError( "[WARN]: frameNo=%d data is not found" )
        target = self.__frames[ frameNo ]
        target.offsetX = offsetX
        target.offsetY = offsetY
        return target

    def getBycycle( self ):
        '''
            Get Bycycle Value
        '''
        if self.__bicycleFlag:
            return 1
        return 0

    def getBus( self ):
        '''
            Get Bus Value
        '''
        return self.__busFlag

    def isInCamera( self, expCamera ):
        '''
            Check Camera
        '''
        if expCamera in self.__cameraSet:
            return 1
        return 0

    def averageDetectConfidence( self ):
        '''
            Get Average Detection Confidence

            @return float: Average of Detection Confidence
        '''
        detectConfList = list(
            map(
                lambda e: e.detectConfidence,
                list( self.__frames.values() )
            )
        )
        return statistics.mean( detectConfList )

    def averageTrackingConfidence( self ):
        '''
            Get Average of Tracking Confidence
        '''
        trackConfList = list(
            map(
                lambda e: e.trackingConfidence,
                list( self.__frames.values() )
            )
        )
        return statistics.mean( trackConfList )

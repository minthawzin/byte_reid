'''
@file data_file_loader.py
 
@author ssugino / created on 2022/1/10
@copyright 2022 GlobalWalkers.inc. All rights reserved.
'''
import os, glob, re, codecs, json

from db.dataset import TrackingDataset

class DataFileLoader( object ):
    '''
        Data File Loader Class
    '''

    def __init__( self ):
        '''
            Constructor
        '''
        super( DataFileLoader, self ).__init__()

    def load( self, dirPath ):
        '''
            Load All Json from specified Directory
            
            @param dirPath: Directory Path that saved Tracking Json Directory
            @yield Tracking Dataset
        '''
        jsonList = glob.glob( "%s/**/*.json" % dirPath, recursive=True )
        jsonList.sort()
        
        for jsonPath in jsonList:
            yield self.parse( jsonPath )

    def parse( self, jsonPath ):
        '''
            Parse Timeline Data from Json
            Set Data to specified dataset instance.

            @param jsonPath: Json File Path
            @param dataset: Target Tracking Dataset Instance
        '''
        jsonName = os.path.basename( jsonPath )
        result = re.match( 
            r"(?P<camera>.+)_(?P<date>.+)_(?P<startTime>.+)-(?P<endTime>.+)\.json", 
            jsonName
        )
        if result is None:
            raise AttributeError( "Invalid File Name format: %s" % jsonName )

        return TrackingDataset(
            result[ 'camera' ],
            result[ 'date' ],
            result[ 'startTime' ],
            result[ 'endTime' ],
            jsonPath
        )

'''
@file json2csv.py
 
@author ssugino / created on 2022/1/24
@copyright 2022 GlobalWalkers.inc. All rights reserved.
'''
import os, sys
HTACH_TECHNOLOGY_SRC_PATH = os.path.dirname( os.path.dirname( __file__ ) )
sys.path.append( HTACH_TECHNOLOGY_SRC_PATH )

from db.json_info import JsonSetList
from converter.csv1_converter import CSV1Records
from converter.csv2_converter import CSV2Records
from converter.csv3_converter import CSV3Records

#------------------------------------------------------------------------------
# Functions
#------------------------------------------------------------------------------

def json2CSV1( jsonSet ):
    
    for camera, jsonInfo in jsonSet.eachInfo():
        csvName = "T1_%s_%s-%s_Camera%s.csv" % (
            jsonInfo.dateStr, 
            jsonInfo.startTimeStr,
            jsonInfo.endTimeStr,
            camera 
        )
        csvPath = os.path.join( jsonInfo.jsonDir, csvName )

        records = CSV1Records( jsonInfo )
        records.convert( csvPath, frameRate = 30 )

def json2CSV2( jsonSet ):
    
    for camera, jsonInfo in jsonSet.eachInfo():
        csvName = "T2_%s_%s-%s_Camera%s.csv" % (
            jsonInfo.dateStr, 
            jsonInfo.startTimeStr,
            jsonInfo.endTimeStr,
            camera
        )
        csvPath = os.path.join( jsonInfo.jsonDir, csvName )

        records = CSV2Records( jsonInfo )
        records.convert( csvPath, frameRate = 30 )

def json2CSV3( jsonSet ):
    
    camAInfo = jsonSet.getJsonInfo( 'A' )
    camBInfo = jsonSet.getJsonInfo( 'B' )

    csvName = "T3_%s_%s-%s.csv" % (
        camAInfo.dateStr, 
        camAInfo.startTimeStr,
        camAInfo.endTimeStr
    )
    csvPath = os.path.join( camAInfo.jsonDir, csvName )
    records = CSV3Records( camAInfo, camBInfo )
    records.convert( csvPath, frameRate = 30 )

#------------------------------------------------------------------------------
# Main Routine
#------------------------------------------------------------------------------
if __name__ == '__main__':

    # dirPath = "/Volumes/SSD_GWM012/ITSP_results"
    dirPath = "/Users/ssugino/Downloads/Intage/Nagoya/Delivery/Sample/24_JAN_demo"

    jsonSetList = JsonSetList( dirPath, camBExt = '_reid' )
    for jsonSet in jsonSetList.eachSet():

        try:
            # camAInfo = jsonSet.getJsonInfo( 'A' )
            # camBInfo = jsonSet.getJsonInfo( 'B' )
            # print( "\tcamA:", camAInfo, " camB:", camBInfo )
            json2CSV1( jsonSet )
            json2CSV2( jsonSet )
            json2CSV3( jsonSet )

        except Exception as e:
            print( e )
            exit( 1 )

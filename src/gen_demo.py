'''
@file gen_demo.py

@author Min Thaw Zin / created on 2022/1/28
@copyright 2022 GlobalWalkers.inc. All rights reserved.
'''
import os
from db.json_info import JsonSetList
from demo.tracking_movie_generator import TrackingMovieGenerator
from demo.reid_movie_generator import ReidMovieGenerator

#------------------------------------------------------------------------------
# Functions
#------------------------------------------------------------------------------

def gen_track_movie( dirPath ):
    '''
        Generate Tracking Demo Movie
    '''

    jsonSetList = JsonSetList( dirPath )
    for jsonSet in jsonSetList.eachSet():
        for camera, jsonInfo in jsonSet.eachInfo():

            dstMovName = "Tracking%s_%s_%s-%s.MP4" % (
                jsonInfo.camera, jsonInfo.dateStr, jsonInfo.startTimeStr, jsonInfo.endTimeStr
            )
            dstMovPath = os.path.join( jsonInfo.jsonDir, dstMovName )

            generator = TrackingMovieGenerator( jsonInfo )
            generator.generate( dstMovPath )

def gen_reid_movie( dirPath ):
    '''
        Generate ReID Demo Movie
    '''

    jsonSetList = JsonSetList( dirPath, camBExt='_reid' )
    for jsonSet in jsonSetList.eachSet():

        dstMovName = "ReID_%s_%s-%s.MP4" % (
            jsonSet.dateStr, jsonSet.startTimeStr, jsonSet.endTimeStr
        )
        dstMovPath = os.path.join( jsonSet.jsonDir, dstMovName )

        generator = ReidMovieGenerator( jsonSet )
        generator.generate( dstMovPath )

#------------------------------------------------------------------------------
# Main Routine
#------------------------------------------------------------------------------
if __name__ == '__main__':

    # @note Need customise your environment
    dirPath = "./reid_files/tracking_json/day2/09-10/"

    gen_track_movie( dirPath )
    gen_reid_movie( dirPath )

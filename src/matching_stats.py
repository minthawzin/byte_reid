'''
@file matching_stats.py

@author ssugino / created on 2022/1/10
@copyright 2022 GlobalWalkers.inc. All rights reserved.
'''
import os, sys
HTACH_TECHNOLOGY_SRC_PATH = os.path.dirname( os.path.dirname( __file__ ) )
sys.path.append( HTACH_TECHNOLOGY_SRC_PATH )

from db.data_file_loader import DataFileLoader

#------------------------------------------------------------------------------
# Functions
#------------------------------------------------------------------------------

def id_stats( jsonPath ):

    loader = DataFileLoader()
    dataset = loader.parse( jsonPath )
    dataset.restore()

    totalCnt = len( dataset.oidList )
    matchCnt = 0
    
    for oid in dataset.oidList:
        if oid < 20000:
            matchCnt += 1

    print( 
        "%s,%d,%d,%.2f %%" % (
            os.path.basename(jsonPath), 
            matchCnt, 
            totalCnt, 
            ( matchCnt/totalCnt ) * 100
        )
    );

    # print( 
    #     "matched / total = %d / %d" % (
    #         matchCnt, totalCnt
    #     )
    # );


#------------------------------------------------------------------------------
# Main Routine
#------------------------------------------------------------------------------
if __name__ == '__main__':

    jsonList = [
        "/Users/ssugino/Downloads/Intage/Nagoya/Report/GW_20220121/Area_Analysis_ID_Match/CameraA_20211118_0800-0810.json",
        "/Users/ssugino/Downloads/Intage/Nagoya/Report/GW_20220121/Area_Analysis_ID_Match/ID_Matched_CamB/CameraB_20211118_0800-0810.json",
        "/Users/ssugino/Downloads/Intage/Nagoya/Report/GW_20220121/21_Jan_Analysis_results/tracking_json/CameraA_20211118_0930-0940.json",
        "/Users/ssugino/Downloads/Intage/Nagoya/Report/GW_20220121/21_Jan_Analysis_results/tracking_json/id_matched/CameraB_20211118_0930-0940_reid.json"
    ]

    for jsonPath in jsonList:
        id_stats( jsonPath )

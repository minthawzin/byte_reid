'''
@file csv2_converter.py
 
@author ssugino / created on 2022/1/24
@copyright 2022 GlobalWalkers.inc. All rights reserved.
'''
import codecs
import datetime

from db.dataset import TrackingDataset

CSV2_HEADER = '''
camera-id,
video-file,
video-frame,
datetime,
bbox-id,
x1,
y1,
x2,
y2,
bbox-conf,
track-id,
track-conf,
reident1-id,
reident1-conf,
area,
is-bus-user,
is-bycycle-user,
reident2-id,
reident2-conf
'''

CSV2_FORMAT = '''
%(camera)s,
%(file)s,
%(frame)d,
%(datetime)s,
%(dataid)s,
%(x1).2f,
%(y1).2f,
%(x2).2f,
%(y2).2f,
%(detectConf).2f,
%(preid)d,
%(trackConf).2f,
%(oid)d,
%(reIDConf).2f,
%(area)s,
%(bus)d,
%(bycycle)d,
%(reid)d,
%(reIDConf).2f
'''

class CSV2Records( object ):

    @property
    def jsonInfo( self ):
        return self.__jsonInfo

    @property
    def dataset( self ):
        return self.__dataset

    def __init__( self, jsonInfo ):
        super( CSV2Records, self ).__init__()
        self.__jsonInfo = jsonInfo
        self.__dataset = TrackingDataset(
            jsonInfo.camera,
            jsonInfo.dateStr,
            jsonInfo.startTimeStr,
            jsonInfo.endTimeStr,
            jsonInfo.jsonPath
        )

    def convert( self, csvPath, frameRate = 30 ):
        self.dataset.restore()
        records = self.__makeRecords( frameRate )
        with codecs.open( csvPath, 'w', encoding='utf-8' ) as fd:
            fd.write( '\n'.join( records ) )
        print( "[CSV2Records]: save %s" % csvPath )

    def __makeRecords( self, frameRate ):

        records = []
        records.append( CSV2_HEADER.replace( '\n', '' ) )

        for timeline in self.dataset.eachTimeline():
            records.append( "" )
            for frameData in timeline.eachData():

                curDatetimeStr = frameData.getDatetime( 
                    self.jsonInfo.baseDatetime, frameRate 
                )
                record = CSV2_FORMAT % {
                    'camera': self.jsonInfo.camera,
                    'file': self.jsonInfo.movName,
                    'frame': frameData.frameNo,
                    'datetime': curDatetimeStr,
                    'dataid': frameData.dataid,
                    'x1': frameData.x1,
                    'y1': frameData.y1,
                    'x2': frameData.x2,
                    'y2': frameData.y2,
                    'detectConf': frameData.detectConfidence,
                    'preid': frameData.preid,
                    'trackConf': frameData.trackingConfidence,
                    'oid': frameData.preid,
                    'reIDConf': frameData.trackingConfidence,
                    'area': frameData.area,
                    'bus': timeline.getBus(),
                    'bycycle': timeline.getBycycle(),
                    'reid': frameData.oid,
                    'reIDConf': frameData.reidConfidence
                }
                record = record.replace( '\n', '' )
                records.append( record )
        
        return records


'''
@file csv3_converter.py
 
@author ssugino / created on 2022/1/24
@copyright 2022 GlobalWalkers.inc. All rights reserved.
'''
import codecs
from db.dataset import TrackingDataset

CSV3_HEADER = '''
reident2-id,
datetime-start,
datetime-end,
area-start,
area-end,
is-bus-user,
is-bycycle-user,
is-in-camera1,
is-in-camera2,
camera-start,
video-file-start,
video-frame-start,
bbox-id-start,
camera-end,
video-file-end,
video-frame-end,
bbox-id-end
'''

CSV3_FORMAT = '''
%(oid)d,
%(startDatetime)s,
%(endDatetime)s,
%(startArea)s,
%(endArea)s,
%(isBus)d,
%(isBycycle)d,
%(isInCameraA)d,
%(isInCameraB)d,
%(startCamera)s,
%(areaStartDatetime)s,
%(startFrame)s,
%(startDataID)s,
%(endCamera)s,
%(areaEndDatetime)s,
%(endFrame)s,
%(endDataID)s
'''

class CSV3Records( object ):

    @property
    def jsonInfo( self ):
        return self.__camAInfo

    @property
    def dataset( self ):
        return self.__dataset

    def __init__( self, camAInfo, camBInfo ):
        
        super( CSV3Records, self ).__init__()
        
        self.__camAInfo = camAInfo
        self.__camBInfo = camBInfo
        
        self.__camADataset = TrackingDataset(
            camAInfo.camera,
            camAInfo.dateStr,
            camAInfo.startTimeStr,
            camAInfo.endTimeStr,
            camAInfo.jsonPath
        )
        self.__camBDataset = TrackingDataset(
            camBInfo.camera,
            camBInfo.dateStr,
            camBInfo.startTimeStr,
            camBInfo.endTimeStr,
            camBInfo.jsonPath
        )

    def convert( self, csvPath, frameRate = 30 ):
        
        self.__camADataset.restore()
        self.__camBDataset.restore()

        self.__camADataset.merge( self.__camBDataset )

        print( "[CSV3Records]: make records start" )
        records = self.__makeRecords( self.__camADataset, frameRate )

        with codecs.open( csvPath, 'w', encoding='utf-8' ) as fd:
            fd.write( '\n'.join( records ) )
        print( "[CSV3Records]: save %s" % csvPath )

    def __makeRecords( self, dataset, frameRate ):

        print( "[CSV3Records]: make record based on Camera%s" % dataset.camera )

        records = []
        records.append( CSV3_HEADER.replace( '\n', '' ) )
        for timeline in dataset.eachTimeline():

            startCamera = timeline.startData.camera
            startArea   = timeline.startData.area
            startFrame  = timeline.startData.frameNo
            startDataID = timeline.startData.dataid
            startDatetimeStr = timeline.startData.getDatetime(
                self.jsonInfo.baseDatetime, frameRate
            )
            areaStartDatetimeStr = "-"
            if timeline.areaStartData is not None:
                startCamera = timeline.areaStartData.camera
                startArea   = timeline.areaStartData.area
                startFrame  = timeline.areaStartData.frameNo
                startDataID = timeline.areaStartData.dataid
                areaStartDatetimeStr = timeline.areaStartData.getDatetime(
                    self.jsonInfo.baseDatetime, frameRate
                )

            endCamera = timeline.endData.camera
            endArea   = timeline.endData.area
            endFrame  = timeline.endData.frameNo
            endDataID = timeline.endData.dataid
            endDatetimeStr = timeline.endData.getDatetime(
                self.jsonInfo.baseDatetime, frameRate
            )
            areaEndDatetimeStr = "-"
            if timeline.areaEndData is not None:
                endCamera = timeline.areaEndData.camera
                endArea   = timeline.areaEndData.area
                endFrame  = timeline.areaEndData.frameNo
                endDataID = timeline.areaEndData.dataid
                areaEndDatetimeStr = timeline.areaEndData.getDatetime(
                    self.jsonInfo.baseDatetime, frameRate
                )

            record = CSV3_FORMAT % {
                'oid': timeline.oid,
                'startDatetime': startDatetimeStr,
                'endDatetime': endDatetimeStr,
                'startArea': startArea,
                'endArea': endArea,
                'isBus': timeline.getBus(),
                'isBycycle': timeline.getBycycle(),
                'isInCameraA': timeline.isInCamera( 'A' ),
                'isInCameraB': timeline.isInCamera( 'B' ),
                'startCamera': startCamera,
                'areaStartDatetime': areaStartDatetimeStr,
                'startFrame': str( startFrame ),
                'startDataID': startDataID,
                'endCamera': endCamera,
                'areaEndDatetime': areaEndDatetimeStr,
                'endFrame': str( endFrame ),
                'endDataID': endDataID
            }
            record = record.replace( '\n', '' )
            records.append( record )
        
        return records
